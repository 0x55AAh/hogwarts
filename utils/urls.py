from __future__ import unicode_literals
from django.utils.functional import SimpleLazyObject
from django.utils import six
import re


def _lazy_re_compile(regex, flags=0):
    """Lazily compile a regex with flags."""
    def _compile():
        # Compile the regex if it was not passed pre-compiled.
        if isinstance(regex, six.string_types):
            return re.compile(regex, flags)
        else:
            assert not flags, "flags must be empty if regex is passed pre-compiled"
            return regex
    return SimpleLazyObject(_compile)


class UrlRegex(object):
    ul = '\u00a1-\uffff'  # unicode letters range (must be a unicode string, not a raw string)

    # IP patterns
    ipv4_re = r'(?:25[0-5]|2[0-4]\d|[0-1]?\d?\d)(?:\.(?:25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}'
    ipv6_re = r'\[[0-9a-f:\.]+\]'  # (simple regex, validated later)

    # Host patterns
    hostname_re = r'[a-z' + ul + r'0-9](?:[a-z' + ul + r'0-9-]{0,61}[a-z' + ul + r'0-9])?'
    # Max length for domain name labels is 63 characters per RFC 1034 sec. 3.1
    domain_re = r'(?:\.(?!-)[a-z' + ul + r'0-9-]{1,63}(?<!-))*'
    tld_re = (
        '\.'  # dot
        '(?!-)'  # can't start with a dash
        '(?:[a-z' + ul + '-]{2,63}'  # domain label
        '|xn--[a-z0-9]{1,59})'  # or punycode label
        '(?<!-)'  # can't end with a dash
        '\.?'  # may have a trailing dot
    )
    host_re = '(' + hostname_re + domain_re + tld_re + '|localhost)'

    build_regex = (
        r'(?:[a-z0-9\.\-\+]*)://'  # scheme
        r'(?:\S+(?::\S*)?@)?'  # user:pass authentication
        r'(?:' + ipv4_re + '|' + ipv6_re + '|' + host_re + ')'
        r'(?::\d{2,5})?'  # port
        r'(?:[/?#][^\s]*)?'  # resource path
    )

    url = _lazy_re_compile(r'^' + build_regex + r'\Z', re.IGNORECASE)
    urls = _lazy_re_compile(r'(?:' + build_regex + r')+', re.IGNORECASE)


url_regex = UrlRegex()
