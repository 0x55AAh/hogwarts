import inspect


class CheckerMixin(object):
    checker_prefix = '_check'

    def _checkers(self):
        return [
            value for name, value
            in inspect.getmembers(self, predicate=inspect.ismethod)
            if name.endswith(self.checker_prefix)
        ]

    def checkers_results(self):
        return [checker() for checker in self._checkers()]
