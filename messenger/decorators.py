def action(**kwargs):
    def decorator(func):
        func.action = True
        func.kwargs = kwargs
        return func
    return decorator
