from channels_api.permissions import BasePermission
from guardian.shortcuts import get_perms
from .models import GroupMembership


class BaseGroupPermission(object):
    def __init__(self, permissions=None, deconstructed_instance=None):
        self.permissions, self.instance = permissions or {}, None
        if deconstructed_instance:
            self.model, self.pk = deconstructed_instance
            if self.model and self.pk:
                try:
                    self.instance = self.model.objects.get(pk=self.pk)
                except self.model.DoesNotExist:
                    pass

    def has_permission(self, sender, action, group_pk):
        pass


class GroupPermission(BaseGroupPermission):
    def has_permission(self, sender, action, group_pk):
        try:
            # First check if sender entered the group
            membership = GroupMembership.objects.get(person=sender, group__pk=group_pk, active=True)
        except GroupMembership.DoesNotExist:
            return False

        # Retrieve group permissions for current user
        perms = set(get_perms(sender.user, membership))

        # Check if sender has a group administration permissions
        if 'group_admin' in perms:
            return True

        # Update group permissions set with an instance permissions
        if self.instance is not None:
            perms |= set(get_perms(sender.user, self.instance))

        perms_required = set(self.permissions.get(action, []))
        return perms_required.issubset(perms)


class IsGroupEntered(BaseGroupPermission):
    def has_permission(self, sender, action, group_pk):
        try:
            GroupMembership.objects.get(person=sender, group__pk=group_pk, active=True)
        except GroupMembership.DoesNotExist:
            return False
        return True


class AllowAny(object):
    def has_permission(self, sender, action, group_pk):
        return True


class IsAuthenticated(BasePermission):
    def has_permission(self, sender, action, group_pk):
        return sender.user.pk and sender.user.is_authenticated
