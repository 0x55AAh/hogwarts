# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-07-30 23:10
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('messenger', '0026_auto_20170731_0201'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='clientmessage',
            options={'ordering': ['-created'], 'permissions': (('update_message', 'Can update message'), ('delete_message', 'Can delete message')), 'verbose_name': 'Message', 'verbose_name_plural': 'Messages'},
        ),
    ]
