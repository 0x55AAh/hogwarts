# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-09 19:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('messenger', '0012_auto_20170607_1836'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='groupmembership',
            name='admin',
        ),
        migrations.AddField(
            model_name='groupmembership',
            name='can_send_message',
            field=models.BooleanField(default=True, verbose_name='Can send message'),
        ),
        migrations.AddField(
            model_name='groupmembership',
            name='superuser',
            field=models.BooleanField(default=False, verbose_name='Superuser'),
        ),
        migrations.AlterField(
            model_name='clientmessage',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Person', verbose_name='Person'),
        ),
        migrations.AlterField(
            model_name='clientmessage',
            name='quoted',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='messenger.ClientMessage', verbose_name='Quoted'),
        ),
        migrations.AlterField(
            model_name='messagestatus',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Person', verbose_name='Person'),
        ),
    ]
