from channels_api import detail_action, list_action
from django.core.paginator import Paginator
from channels_api.settings import api_settings
from rest_framework.exceptions import ValidationError
from channels import Group


class PatchModelMixin(object):

    @detail_action()
    def patch(self, pk, data, **kwargs):
        instance = self.get_object_or_404(pk)
        serializer = self.get_serializer(instance, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_patch(serializer)
        return instance, serializer.data, 200

    def perform_patch(self, serializer):
        return serializer.save()


class CreateModelMixin(object):
    """Mixin class that handles the creation of an object using a DRF serializer."""

    @list_action()
    def create(self, data, **kwargs):
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        return instance, serializer.data, 201

    def perform_create(self, serializer):
        return serializer.save()


class RetrieveModelMixin(object):

    @detail_action()
    def retrieve(self, pk, **kwargs):
        instance = self.get_object_or_404(pk)
        serializer = self.get_serializer(instance)
        return instance, serializer.data, 200


class ListModelMixin(object):

    @list_action()
    def list(self, data, **kwargs):
        if not data:
            data = {}
        queryset = self.filter_queryset(self.get_queryset())
        paginator = Paginator(queryset, api_settings.DEFAULT_PAGE_SIZE)
        data = paginator.page(data.get('page', 1))
        serializer = self.get_serializer(data, many=True)
        return queryset, serializer.data, 200


class UpdateModelMixin(object):

    @detail_action()
    def update(self, pk, data, **kwargs):
        instance = self.get_object_or_404(pk)
        serializer = self.get_serializer(instance, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return instance, serializer.data, 200

    def perform_update(self, serializer):
        return serializer.save()


class DeleteModelMixin(object):

    @detail_action()
    def delete(self, pk, **kwargs):
        instance = self.get_object_or_404(pk)
        serializer = self.get_serializer(instance)
        self.perform_delete(instance)
        return instance, serializer.data, 200

    def perform_delete(self, instance):
        return instance.delete()


class SubscribeModelMixin(object):

    @detail_action()
    def subscribe(self, pk, data, **kwargs):
        if 'action' not in data:
            raise ValidationError('action required')
        action = data['action']
        group_name = self._group_name(action, id=pk)
        Group(group_name).add(self.message.reply_channel)
        return None, {'action': action}, 200
