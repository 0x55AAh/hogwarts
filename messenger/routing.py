from channels import route_class
from .consumers import MainDemultiplexer


channel_routing = [
    route_class(MainDemultiplexer, path='^/message/$'),
]
