from channels.generic.websockets import JsonWebsocketConsumer, WebsocketDemultiplexer
from messenger.permissions import GroupPermission
from .decorators import action
from .bindings import (
    TextMessageBinding, MessageStatusBinding, GroupMembershipBinding,
    UrlMessageBinding, FileMessageBinding, ImageMessageBinding, PersonNetworkStatusBinding
)
import logging

logger = logging.getLogger(__name__)


class BaseMessagesConsumer(JsonWebsocketConsumer):
    permission_classes = None
    permissions = None
    actions = None

    def __new__(cls, message, multiplexer=None):
        consumer = super(BaseMessagesConsumer, cls).__new__(cls)

        consumer.available_actions = {}

        for methodname in dir(consumer):
            attr = getattr(consumer, methodname)
            is_action = getattr(attr, 'action', False)
            if is_action:
                kwargs = getattr(attr, 'kwargs', {})
                name = kwargs.get('name', methodname)
                consumer.available_actions[name] = methodname

        for name in (consumer.actions or []):
            @action()
            def method(self, _multiplexer, sender, group, _message):
                self._send(_multiplexer, sender, group, _message)
            setattr(cls, name, method)
            consumer.available_actions[name] = name

        return consumer

    def __init__(self, message, multiplexer=None):
        super(BaseMessagesConsumer, self).__init__(message, multiplexer=multiplexer)
        self.check_permissions()
        self.sender = None

    def check_permissions(self):
        permissions = self.permissions or {}
        actions = set(permissions.keys())
        available_actions = set(self.available_actions.keys())
        if not actions.issubset(available_actions):
            raise ValueError('Permissions has an unusable actions')

    def get_permission_classes(self, action=None):
        return self.permission_classes or []

    def has_permission(self, sender, action, group):
        for cls in self.get_permission_classes():
            if not cls(self.permissions).has_permission(sender, action, group):
                return False
        return True

    def run_action(self, action, multiplexer, sender, group, **kwargs):
        if not self.has_permission(sender, action, group):
            logger.debug('Permission denied: sender={}, action={}, group={}'.format(sender, action, group))
        elif action not in self.available_actions:
            logger.debug('Invalid action: sender={}, action={}, group={}'.format(sender, action, group))
        else:
            methodname = self.available_actions[action]
            method = getattr(self, methodname)
            message = {'action': methodname}
            if 'text' in kwargs:
                message['value'] = kwargs['text']
            method(multiplexer, sender, group, message)

    def receive(self, content, multiplexer=None, **kwargs):
        action, group = content.get('action'), content.get('group')

        if not action:
            raise ValueError('Action must not be empty')
        if not group:
            raise ValueError('Group id must not be empty')

        self.sender = self.message.user.get_current_profile()
        self.run_action(action, multiplexer, self.sender, group, text=content.get('text'))

    def _build_payload(self, sender, group, message):
        from api.v1.messenger.serializers import PersonSerializer
        sender_serializer = PersonSerializer(sender)
        payload = {
            'sender': sender_serializer.data,
            'group': group,
            'action': message['action']
        }
        if 'value' in message:
            payload['value'] = message['value']

        return payload

    def _send(self, multiplexer, sender, group, message):
        payload = self._build_payload(sender, group, message)
        multiplexer.group_send(name=str(group), stream=multiplexer.stream, payload=payload)
        logger.debug('Send message: {}'.format(payload))

    def _reply(self, multiplexer, sender, group, message):
        payload = self._build_payload(sender, group, message)
        multiplexer.send(payload=payload)
        logger.debug('Send message: {}'.format(payload))


class SystemMessagesConsumer(BaseMessagesConsumer):
    permission_classes = [GroupPermission]
    permissions = {
        'typing': ['add_message'],
        'stop_typing': ['add_message'],
        'sending_file': ['add_message'],
        'sending_image': ['add_message'],
        'ping': ['add_message'],
    }
    actions = [
        'error', 'warning',
        'typing', 'stop_typing',
        'muted', 'un_muted',
        'sending_file', 'sending_image',
        'read'
    ]

    @action()
    def ping(self, multiplexer, sender, group, message):
        message['value'] = 'pong'
        self._reply(multiplexer, sender, group, message)


class ClientMessagesConsumer(BaseMessagesConsumer):
    permission_classes = None
    permissions = None


class MainDemultiplexer(WebsocketDemultiplexer):
    http_user_and_session = True

    consumers = {
        'system-message': SystemMessagesConsumer,

        # Client messages consumers
        'text-message': TextMessageBinding.consumer,
        'url-message': UrlMessageBinding.consumer,
        'file-message': FileMessageBinding.consumer,
        'image-message': ImageMessageBinding.consumer,

        'message-status': MessageStatusBinding.consumer,
        'group-membership': GroupMembershipBinding.consumer,
        'profile-network-status': PersonNetworkStatusBinding.consumer,
        # 'group-member': GroupMemberBinding.consumer,
        # 'group': GroupBinding.consumer,
    }

    def receive(self, content, **kwargs):
        super(MainDemultiplexer, self).receive(content, **kwargs)
        sender = self.message.user.get_current_profile()
        sender.network_status.online()

    def connection_groups(self, **kwargs):
        if self.message.user.is_anonymous():
            return []
        sender = self.message.user.get_current_profile()
        groups = sender.message_groups.values_list('pk', flat=True)
        return list(map(str, groups)) + [sender.channels_group.name]

    def connect(self, message, **kwargs):
        if self.message.user.is_anonymous():
            self.message.reply_channel.send({'accept': False})
        super(MainDemultiplexer, self).connect(message, **kwargs)
