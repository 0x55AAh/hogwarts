from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from .models import (
    Group, GroupMembership, MessageStatus,
    TextMessage, UrlMessage, FileMessage, ImageMessage
)


@admin.register(Group)
class GroupAdmin(GuardedModelAdmin):
    list_display = ['name', 'members_count', 'created', 'updated']

    def members_count(self, obj):
        return obj.memberships.count()

    class MembersInline(admin.TabularInline):
        model = GroupMembership
        extra = 0

    inlines = [MembersInline]


class ClientMessageAdmin(GuardedModelAdmin):
    list_display = ['value', 'sender', 'group', 'draft']
    list_select_related = ['sender', 'institution', 'group', 'quoted']
    list_filter = ['draft']

    class MessageStatusesInline(admin.TabularInline):
        model = MessageStatus
        extra = 0

    inlines = [MessageStatusesInline]


@admin.register(TextMessage)
class TextMessageAdmin(ClientMessageAdmin):
    pass


@admin.register(UrlMessage)
class UrlMessageAdmin(ClientMessageAdmin):
    pass


@admin.register(FileMessage)
class FileMessageAdmin(ClientMessageAdmin):
    pass


@admin.register(ImageMessage)
class ImageMessageAdmin(ClientMessageAdmin):
    pass
