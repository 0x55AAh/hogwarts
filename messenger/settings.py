from django.conf import settings


PERSONAL_GROUP_PREFIX = getattr(settings, 'MESSENGER_PERSONAL_GROUP_PREFIX', 'pg')
