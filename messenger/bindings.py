from channels.binding.websockets import WebsocketBinding
from channels_api.bindings import ResourceBinding, ResourceBindingBase
from channels import Group as ChannelsGroup
from guardian.shortcuts import assign_perm

from api.v1.messenger.serializers import (
    MessageSerializer, TextMessageSerializer, UrlMessageSerializer,
    FileMessageSerializer, ImageMessageSerializer, MessageWithNestedSenderSerializer,
    MessageStatusSerializer, GroupSerializer
)
from .permissions import GroupPermission
from .mixins import (
    PatchModelMixin, CreateModelMixin, RetrieveModelMixin, ListModelMixin,
    UpdateModelMixin, DeleteModelMixin, SubscribeModelMixin
)
from .models import (
    GroupMembership, Group, ClientMessage, MessageStatus,
    TextMessage, UrlMessage, FileMessage, ImageMessage
)
from core.models import Person, Teacher, Pupil, Student, PersonNetworkStatus
from rest_framework.exceptions import APIException
import json


CREATE = 'create'
UPDATE = 'update'
DELETE = 'delete'
PATCH = 'patch'


class ResourceBindingBaseCustom(ResourceBindingBase):
    permission_classes = None
    # mark as abstract
    model = None
    permissions = None
    default_instance_permissions = None
    receivers_enabled = False

    def __init__(self):
        self.sender = None
        self.group = None
        self.request_id = None
        self.pk = None

    def get_queryset(self):
        return self.model.objects.all()

    def filter_queryset(self, queryset):
        return queryset.filter(institution=self.sender.institution, active=True)

    def serialize(self, instance, action):
        payload = super(ResourceBindingBaseCustom, self).serialize(instance, action)
        payload.update(sender=self.sender.pk, group=self.group)
        return payload

    def deserialize(self, message):
        body = json.loads(message['text'])
        self.request_id = body.get('request_id')
        action = body['action']
        self.pk = pk = body.get('pk')
        data = body.get('data', {})
        self.sender = message.user.get_current_profile()
        self.group = body.get('group') or data['group']
        data.update(
            sender=self.sender.pk,
            institution=self.sender.institution.pk
        )
        return action, pk, data

    def get_permission_classes(self, action=None):
        return self.permission_classes or []

    def has_permission(self, sender, action, group):
        for cls in self.get_permission_classes():
            permission_object = cls(self.permissions, deconstructed_instance=(self.model, self.pk))
            if not permission_object.has_permission(sender, action, group):
                return False
        return True

    def data_on_action_failure(self, sender, group):
        from api.v1.messenger.serializers import PersonSerializer
        sender_serializer = PersonSerializer(sender)
        payload = {
            'sender': sender_serializer.data,
            'group': group
        }
        return payload

    def run_action(self, action, pk, data):
        try:
            if not self.has_permission(self.sender, action, self.group):
                self.reply(
                    action, data=self.data_on_action_failure(self.sender, self.group),
                    errors=['Permission Denied'], status=401, request_id=self.request_id
                )
            elif action not in self.available_actions:
                self.reply(
                    action, data=self.data_on_action_failure(self.sender, self.group),
                    errors=['Invalid Action'], status=400, request_id=self.request_id
                )
            else:
                methodname = self.available_actions[action]
                method = getattr(self, methodname)
                detail = getattr(method, 'detail', True)
                if detail:
                    rv = method(pk, data=data)
                else:
                    rv = method(data=data)
                obj, data, status = rv
                self.action_extra(action, obj)
                self.reply(action, data=data, status=status, request_id=self.request_id)
                if not self.receivers_enabled:
                    self.post_change(action, obj, data.get('id'))
        except APIException as ex:
            self.reply(
                action,
                errors=self._format_errors(ex.detail),
                status=ex.status_code,
                request_id=self.request_id
            )

    def action_extra(self, action, obj):
        """
        Run additional ops while executing action.
        """
        if action == CREATE:
            self.assign_permissions(obj)

    def assign_permissions(self, obj):
        for perm in (self.default_instance_permissions or []):
            assign_perm(perm, self.sender.user, obj)

    def reply(self, action, data=None, errors=None, status=200, request_id=None):
        """
        Helper method to send a encoded response to the message's reply_channel.
        """
        payload = {
            'errors': errors or [],
            'data': data,
            'action': action,
            'response_status': status,
            'sender': self.sender.pk,
            'group': self.group,
            'request_id': request_id
        }
        return self.message.reply_channel.send(self.encode(self.stream, payload))

    def post_change(self, action, obj, pk):
        if action not in [CREATE, UPDATE, PATCH, DELETE]:
            return  # not allowed action, bail.

        if isinstance(obj, self.model):
            group_names = self.group_names(obj, action)

            if not group_names:
                return  # no need to serialize, bail.
            payload = self.serialize(obj, action)
            if payload == {}:
                return  # nothing to send, bail.
            payload.update(pk=pk)

            assert self.stream is not None

            message = self.encode(self.stream, payload)
            for group_name in group_names:
                group = ChannelsGroup(group_name)
                group.send(message)

    @classmethod
    def pre_save_receiver(cls, instance, **kwargs):
        if cls.receivers_enabled:
            super(ResourceBindingBaseCustom, cls).pre_save_receiver(instance, **kwargs)

    @classmethod
    def post_save_receiver(cls, instance, created, **kwargs):
        if cls.receivers_enabled:
            super(ResourceBindingBaseCustom, cls).post_save_receiver(instance, created, **kwargs)

    @classmethod
    def pre_delete_receiver(cls, instance, **kwargs):
        if cls.receivers_enabled:
            super(ResourceBindingBaseCustom, cls).pre_delete_receiver(instance, **kwargs)

    @classmethod
    def post_delete_receiver(cls, instance, **kwargs):
        if cls.receivers_enabled:
            super(ResourceBindingBaseCustom, cls).post_delete_receiver(instance, **kwargs)


class ResourceBindingCustom(PatchModelMixin, CreateModelMixin, RetrieveModelMixin, ListModelMixin,
                            UpdateModelMixin, DeleteModelMixin, SubscribeModelMixin, ResourceBindingBaseCustom):
    # mark as abstract
    model = None
    model_serializer_class = None


class MessageBinding(ResourceBindingCustom):
    model = ClientMessage
    serializer_class = MessageSerializer
    model_serializer_class = MessageWithNestedSenderSerializer
    permission_classes = [GroupPermission]
    stream = 'client-message'
    permissions = {
        'create': ['add_message'],
        'patch': ['change_message'],
        'update': ['change_message'],
        'delete': ['delete_message'],
    }
    default_instance_permissions = [
        'change_message', 'delete_message'
    ]

    @classmethod
    def group_names(cls, instance, action):
        return [str(instance.group.pk)]

    def serialize_data(self, instance):
        return self.model_serializer_class(instance).data

    def post_change(self, action, instance, pk):
        if not instance.draft:
            super(MessageBinding, self).post_change(action, instance, pk)

    def reply(self, action, data=None, errors=None, status=200, request_id=None):
        if data and 'draft' in data and data['draft']:
            payload = {
                'errors': errors or [],
                'data': data,
                'action': action,
                'response_status': status,
                'sender': self.sender.pk,
                'group': self.group,
                'request_id': request_id
            }
            return self.sender.channels_group.send(self.encode(self.stream, payload))
        else:
            return super(MessageBinding, self).reply(action, data, errors, status, request_id)


class TextMessageBinding(MessageBinding):
    model = TextMessage
    stream = 'text-message'
    serializer_class = TextMessageSerializer


class UrlMessageBinding(MessageBinding):
    model = UrlMessage
    stream = 'url-message'
    serializer_class = UrlMessageSerializer


class FileMessageBinding(MessageBinding):
    model = FileMessage
    stream = 'file-message'
    serializer_class = FileMessageSerializer


class ImageMessageBinding(MessageBinding):
    model = ImageMessage
    stream = 'image-message'
    serializer_class = ImageMessageSerializer


class GroupBinding(ResourceBindingCustom):
    model = Group
    serializer_class = GroupSerializer
    stream = 'group'
    permission_classes = []
    permissions = {
        'create': ['add_group'],
        'patch': ['change_group'],
        'update': ['change_group'],
        'delete': ['delete_group'],
    }
    default_instance_permissions = [
        'change_group', 'delete_group'
    ]

    @classmethod
    def group_names(cls, instance, action):
        return [str(instance.pk)]

    def has_permission(self, user, action, pk):
        return True


class GroupMembershipBinding(WebsocketBinding):
    model = GroupMembership
    stream = 'group-membership'
    fields = ['__all__']

    @classmethod
    def group_names(cls, instance):
        return [str(instance.group.pk)]

    def has_permission(self, user, action, pk):
        return True


class GroupMemberBinding(WebsocketBinding):
    model = Person
    stream = 'group-member'
    fields = ['__all__']

    @classmethod
    def group_names(cls, instance):
        groups = instance.message_memberships \
            .filter(active=True).values_list('group__pk', flat=True)
        return map(str, groups)

    def has_permission(self, user, action, pk):
        return True


class TeacherMemberBinding(GroupMemberBinding):
    model = Teacher


class PupilMemberBinding(GroupMemberBinding):
    model = Pupil


class StudentMemberBinding(GroupMemberBinding):
    model = Student


class MessageStatusBinding(ResourceBinding):
    model = MessageStatus
    stream = 'message-status'
    serializer_class = MessageStatusSerializer
    queryset = MessageStatus.objects.all()

    @classmethod
    def group_names(cls, instance, action):
        return [str(instance.message.group.pk)]

    def has_permission(self, user, action, pk):
        if action == 'update':
            return True
        return False


class PersonNetworkStatusBinding(WebsocketBinding):
    model = PersonNetworkStatus
    stream = 'profile-network-status'
    fields = ['__all__']

    @classmethod
    def group_names(cls, instance):
        groups = instance.person.message_memberships\
            .filter(active=True).values_list('group__pk', flat=True)
        return map(str, groups)

    def has_permission(self, user, action, pk):
        return True
