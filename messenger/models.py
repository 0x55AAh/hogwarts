from __future__ import unicode_literals
from colorfield.fields import ColorField
from django.dispatch import receiver
from core.models import Person
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from polymorphic.models import PolymorphicModel
from utils.urls import url_regex
from channels import Group as ChannelsGroup
from django.db.models.signals import post_save, post_delete
from model_utils import Choices
import os
import logging


logger = logging.getLogger(__name__)


@python_2_unicode_compatible
class MessageStatus(models.Model):
    STATUS = Choices(
        ('sent', _('Sent')),
        ('read', _('Read'))
    )
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE)
    message = models.ForeignKey('ClientMessage', verbose_name=_('Message'), related_name='statuses',
                                on_delete=models.CASCADE)
    person = models.ForeignKey('core.Person', verbose_name=_('Person'), on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now=True)
    value = models.CharField(max_length=32, verbose_name=_('Value'), choices=STATUS, default=STATUS.sent)

    def __str__(self):
        return '{}:{}'.format(self.message, self.value)

    class Meta:
        verbose_name = _('Message Status')
        verbose_name_plural = _('Message Statuses')


class BaseMessage(PolymorphicModel):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE)
    group = models.ForeignKey('Group', verbose_name=_('Group'), related_name='%(class)ss', on_delete=models.CASCADE)
    sender = models.ForeignKey('core.Person', verbose_name=_('Sender'), null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    class Meta:
        abstract = True
        ordering = ['created']


class BaseClientMessage(BaseMessage):
    draft = models.BooleanField(default=False, verbose_name=_('Draft'))
    updated = models.DateTimeField(verbose_name=_('Updated'), null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.pk:
            self.updated = timezone.now()
        self.group.updated = timezone.now()
        super(BaseClientMessage, self).save(force_insert, force_update, using, update_fields)

    @classmethod
    def outgoing_messages(cls, person):
        return cls.objects.filter(active=True, sender=person)

    @classmethod
    def incoming_messages(cls, person):
        return cls.objects.filter(active=True, statuses__person=person)

    @classmethod
    def draft_messages(cls, person):
        return cls.outgoing_messages(person).filter(draft=True)

    @classmethod
    def new_messages(cls, person):
        return cls.incoming_messages(person).exclude(statuses__value=MessageStatus.STATUS.read)

    class Meta(BaseMessage.Meta):
        ordering = ['-created']
        abstract = True


def message_files(instance, filename):
    return os.path.join(
        'message_files', str(instance.institution.pk),
        str(instance.sender.user.pk), filename
    )


class ClientMessage(BaseClientMessage):
    quoted = models.ForeignKey('self', verbose_name=_('Quoted'), on_delete=models.CASCADE, null=True, blank=True)

    def get_status(self, person):
        if self.sender == person:
            return None
        return self.statuses.get(person=person)

    class Meta(BaseClientMessage.Meta):
        verbose_name = _('Message')
        verbose_name_plural = _('Messages')
        permissions = (
            ('change_message', _('Can change message')),
            ('delete_message', _('Can delete message'))
        )


@python_2_unicode_compatible
class BaseFileMessage(ClientMessage):
    value = models.FileField(verbose_name=_('Value'), upload_to=message_files)
    caption = models.TextField(verbose_name=_('Caption'), null=True)

    def __str__(self):
        return self.value.path

    class Meta(ClientMessage.Meta):
        abstract = True


@python_2_unicode_compatible
class TextMessage(ClientMessage):
    value = models.TextField(verbose_name=_('Value'))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.create_url_messages()
        super(TextMessage, self).save(force_insert, force_update, using, update_fields)

    def retrieve_urls(self):
        return url_regex.urls.findall(self.value)

    def create_url_messages(self):
        messages = [
            UrlMessage(institution=self.institution, value=url)
            for url in self.retrieve_urls()
        ]
        # raise ValueError("Can't bulk create a multi-table inherited model")
        # self.__class__.objects.bulk_create(messages)
        for message in messages:
            message.save()

    def __str__(self):
        return self.value

    class Meta(ClientMessage.Meta):
        verbose_name = _('Text message')
        verbose_name_plural = _('Text messages')


class UrlMessage(ClientMessage):
    value = models.URLField(max_length=1024, verbose_name=_('Value'))

    def __str__(self):
        return self.value

    class Meta(ClientMessage.Meta):
        verbose_name = _('URL message')
        verbose_name_plural = _('URL messages')


class FileMessage(BaseFileMessage):
    class Meta(BaseFileMessage.Meta):
        verbose_name = _('File message')
        verbose_name_plural = _('File messages')


class ImageMessage(BaseFileMessage):
    class Meta(BaseFileMessage.Meta):
        verbose_name = _('Image message')
        verbose_name_plural = _('Image messages')


@python_2_unicode_compatible
class Group(models.Model):
    TYPE = Choices(
        ('p', _('Personal')),
        ('m', _('Multiple')),
        ('c', _('Channel')),
    )
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    members = models.ManyToManyField('core.Person', through='GroupMembership', related_name='message_groups')
    topic = models.CharField(max_length=255, verbose_name=_('Topic'), null=True, blank=True)
    image = models.FileField(verbose_name=_('Image'), upload_to=message_files, null=True, blank=True)
    type = models.CharField(max_length=128, verbose_name=_('Type'), choices=TYPE, default=TYPE.m)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(verbose_name=_('Updated'), null=True, blank=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return self.name

    @classmethod
    def parse_gid(cls, gid):
        if gid is None or gid is '':
            return None, None
        gid = str(gid)
        if gid.isdigit():
            return None, gid
        else:
            _type, _id = gid[0], gid[1:]
            base_error_message = 'Group id not valid.'
            if _type not in map(lambda x: x[0], cls.TYPE):
                raise ValueError('{base} Group type error: {type}'.format(type=_type, base=base_error_message))
            elif not _id.isdigit():
                raise ValueError('{base} Group id error: {id}'.format(id=_id, base=base_error_message))
            else:
                return _type, _id

    @classmethod
    def get_group_by_gid(cls, gid, request):
        _type, _id = cls.parse_gid(gid)
        group = None
        if _id:
            kwargs = {'pk': _id}
            if _type:
                kwargs.update({'type': _type})
                if _type == cls.TYPE.p:
                    try:
                        person = Person.objects.get(pk=_id)
                    except Person.DoesNotExist:
                        pass
                    else:
                        try:
                            group = cls.objects \
                                .filter(members__in=[request.profile]) \
                                .filter(members__in=[person]) \
                                .get(type=_type)
                        except cls.DoesNotExist:
                            pass
                    return group
            try:
                group = cls.objects.get(**kwargs)
            except cls.DoesNotExist:
                pass
        return group

    @property
    def memberships(self):
        return self.message_memberships.filter(active=True)

    @property
    def drafts(self):
        return self.clientmessages.filter(active=True, draft=True)

    @property
    def messages(self):
        return self.clientmessages.filter(active=True, draft=False)

    @property
    def image_messages(self):
        return self.imagemessages.filter(active=True, draft=False)

    @property
    def file_messages(self):
        return self.filemessages.filter(active=True, draft=False)

    @property
    def text_messages(self):
        return self.textmessages.filter(active=True, draft=False)

    @property
    def url_messages(self):
        return self.urlmessages.filter(active=True, draft=False)

    @property
    def channels_group(self):
        return ChannelsGroup(str(self.pk))

    def disconnect(self):
        self.channels_group.send({'close': True})

    # def create_file_message(self, person, message, message_model, draft=False):
    #     from django.core.files.storage import default_storage
    #     obj = message_model.objects.create(institution=self.institution, group=self, sender=person, draft=draft)
    #     with default_storage.open(message) as f:
    #         obj.value.save(os.path.basename(f.name), f, save=True)
    #     return obj

    def subscribe(self, sender, active=False):
        GroupMembership.objects.create(
            institution=self.institution,
            person=sender,
            group=self,
            active=active,
            color=sender.color
        )

    def leave(self, sender):
        memberships = self.memberships
        memberships.get(person=sender).delete()
        if not memberships.exists():
            self.delete()

    def history_clean(self):
        self.messages.delete()

    class Meta:
        ordering = ['updated']
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')
        permissions = (
            # ('add_group', _('Can add group')),
            # ('change_group', _('Can change group')),
            # ('delete_group', _('Can delete group')),
        )


@python_2_unicode_compatible
class GroupMembership(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE)
    person = models.ForeignKey('core.Person', verbose_name=_('Person'), related_name='message_memberships',
                               on_delete=models.CASCADE)
    group = models.ForeignKey('Group', verbose_name=_('Group'), related_name='message_memberships',
                              on_delete=models.CASCADE)
    color = ColorField(default='#47bac1')
    joined = models.DateTimeField(auto_now_add=True)
    muted = models.BooleanField(verbose_name=_('Muted'), default=False)
    last_login = models.DateTimeField(verbose_name=_('Last login'), null=True, blank=True)

    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return '[{}, {}]'.format(self.person, self.group)

    class Meta:
        ordering = ['group__updated']
        verbose_name = _('GroupMembership')
        verbose_name_plural = _('GroupMemberships')
        permissions = (
            ('leave_group', _('Can leave group')),  # The same as delete_groupmembership

            ('clean_history', _('Can clean history')),
            ('add_message', _('Can add message')),

            ('add_member', _('Can add member')),
            ('delete_member', _('Can delete member')),

            ('group_admin', _('Can administrate group')),
        )

    # def add_member(self, person):
    #     has_perms, perm_issues = self.check_perms(['can_add_members'])
    #     if has_perms:
    #         return self.__class__.objects.create(person=person, group=self.group)
    #     else:
    #         logger.debug('Person {person} has no permissions {perms} on group {group}'.format(
    #             person=self.person, perms=','.join(perm_issues), group=self.group.name
    #         ))

    # def remove_member(self, person):
    #     has_perms, perm_issues = self.check_perms(['can_remove_members'])
    #     if has_perms:
    #         membership = self.group.message_memberships.get(person=person)
    #         return membership.delete()
    #     else:
    #         logger.debug('Person {person} has no permissions {perms} on group {group}'.format(
    #             person=self.person, perms=','.join(perm_issues), group=self.group.name
    #         ))

    @property
    def outgoing_messages(self):
        return self.group.messages.filter(active=True, sender=self.person)

    @property
    def incoming_messages(self):
        return self.group.messages.exclude(active=True, sender=self.person)

    @property
    def draft_messages(self):
        return self.group.drafts.filter(active=True, sender=self.person)

    @property
    def new_messages(self):
        return self.incoming_messages.exclude(statuses__value=MessageStatus.STATUS.read)


@receiver(post_save, sender=GroupMembership, dispatch_uid='create_group_membership_receiver')
def create_group_membership_receiver(sender, instance, *args, **kwargs):
    if kwargs['created']:
        instance.person.disconnect()


@receiver(post_delete, sender=GroupMembership, dispatch_uid='remove_group_membership_receiver')
def remove_group_membership_receiver(sender, instance, *args, **kwargs):
    instance.person.disconnect()


def on_create_message(sender, instance, *args, **kwargs):
    if kwargs['created']:
        statuses = [
            MessageStatus(institution=instance.institution, message=instance, person=member)
            for member in instance.group.members.exclude(pk=instance.sender.pk)
        ]
        MessageStatus.objects.bulk_create(statuses)


post_save.connect(on_create_message, sender=TextMessage)
post_save.connect(on_create_message, sender=UrlMessage)
post_save.connect(on_create_message, sender=FileMessage)
post_save.connect(on_create_message, sender=ImageMessage)
