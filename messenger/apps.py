from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class MessengerConfig(AppConfig):
    name = 'messenger'
    verbose_name = _('Messenger')
