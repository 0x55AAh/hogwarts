from django.conf.urls import url, include
from .views import UploadProgressDataView, UploadProgressCreateView


upload_progress_bar = [
    url(r'^$', UploadProgressDataView.as_view(), name='upload-progress-data'),
    url(r'^create/$', UploadProgressCreateView.as_view(), name='upload-progress-create'),
]

urlpatterns = [
    url(r'^upload-progress/', include(upload_progress_bar)),
]
