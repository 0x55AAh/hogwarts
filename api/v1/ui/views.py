from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions
from django.core.cache import cache
from ipware.ip import get_ip
import uuid


class UploadProgressDataView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        progress_id = None
        if 'X-Progress-ID' in self.request.GET:
            progress_id = self.request.GET['X-Progress-ID']
        elif 'X-Progress-ID' in self.request.META:
            progress_id = self.request.META['X-Progress-ID']
        if progress_id:
            cache_key = '%s_%s' % (get_ip(request), progress_id)
            data = cache.get(cache_key)
            return Response(dict(data))
        return Response({
            'error': 'You must provide X-Progress-ID header or query param.'
        })


class UploadProgressCreateView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        data = {'progress_id': uuid.uuid4()}
        return Response(data)
