from rest_framework import generics

from .serializers import InstitutionSerializer
from core.models import EducationalInstitution


class InstitutionDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = EducationalInstitution.objects.filter(active=True)
    serializer_class = InstitutionSerializer

    def get_queryset(self):
        queryset = super(InstitutionDetailView, self).get_queryset()
        queryset = self.get_serializer_class().setup_eager_loading(queryset)
        return queryset


class InstitutionListView(generics.ListCreateAPIView):
    queryset = EducationalInstitution.objects.filter(active=True)
    serializer_class = InstitutionSerializer
