from django.conf.urls import url
from ..institutions.views import (
    InstitutionDetailView, InstitutionListView
)

urlpatterns = [
    url(r'^$', InstitutionListView.as_view(), name='institutions'),
    url(r'^(?P<pk>[0-9]+)/$', InstitutionDetailView.as_view(), name='institutions'),
]
