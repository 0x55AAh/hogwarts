from rest_framework import serializers
from core.models import (
    InstitutionToInstitutionPartnership, InstitutionToInstitutionInvitation,
    Email, Phone, Campus, Person, EducationalInstitution, HighSchool, School,
    LectureRoom, HighSchoolDepartment
)
from copy import copy


class EagerLoadingMixin(object):
    @classmethod
    def setup_eager_loading(cls, queryset):
        if hasattr(cls, 'SELECT_RELATED_FIELDS'):
            queryset = queryset.select_related(*cls.SELECT_RELATED_FIELDS)
        if hasattr(cls, 'PREFETCH_RELATED_FIELDS'):
            queryset = queryset.prefetch_related(*cls.PREFETCH_RELATED_FIELDS)
        return queryset


class HighSchoolDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = HighSchoolDepartment
        exclude = ['institution']


class PhoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phone
        exclude = ['institution']


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        exclude = ['institution']


class CampusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Campus
        exclude = ['institution']


class LectureRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = LectureRoom
        exclude = ['institution']


class PersonSerializer(serializers.ModelSerializer):
    emails = EmailSerializer(many=True)
    phones = PhoneSerializer(many=True)

    class Meta:
        model = Person
        exclude = ['institution', 'polymorphic_ctype']


class BaseInstitutionSerializer(serializers.ModelSerializer):
    emails = EmailSerializer(many=True)
    phones = PhoneSerializer(many=True)
    campuses = CampusSerializer(many=True, source='get_campuses')
    lecture_rooms = LectureRoomSerializer(many=True, source='get_lecture_rooms')
    peoples = PersonSerializer(many=True, source='get_peoples')

    class Meta:
        model = EducationalInstitution
        fields = [
            'id', 'name', 'slug', 'rating', 'emails', 'phones', 'peoples',
            'created', 'updated', 'color', 'active', 'campuses', 'lecture_rooms'
        ]


class InvitedInstitutionSerializer(BaseInstitutionSerializer):
    class Meta(BaseInstitutionSerializer.Meta):
        pass

    def to_representation(self, instance):
        if isinstance(instance, HighSchool):
            return InvitedHighSchoolSerializer(instance, context=self.context).to_representation(instance)
        if isinstance(instance, School):
            return InvitedSchoolSerializer(instance, context=self.context).to_representation(instance)
        return super(InvitedInstitutionSerializer, self).to_representation(instance)


class InvitedHighSchoolSerializer(BaseInstitutionSerializer):
    departments = HighSchoolDepartmentSerializer(many=True)

    class Meta(BaseInstitutionSerializer.Meta):
        model = HighSchool
        fields = copy(BaseInstitutionSerializer.Meta.fields)
        fields += [
            'departments', 'accreditation'
        ]


class InvitedSchoolSerializer(BaseInstitutionSerializer):
    class Meta(BaseInstitutionSerializer.Meta):
        model = School
        fields = copy(BaseInstitutionSerializer.Meta.fields)
        fields += [

        ]


class PartnershipSerializer(serializers.ModelSerializer):
    partner = InvitedInstitutionSerializer()

    class Meta:
        model = InstitutionToInstitutionPartnership
        fields = [
            'id', 'created', 'updated', 'opened', 'partner'
        ]


class OutgoingInvitationSerializer(serializers.ModelSerializer):
    destination = InvitedInstitutionSerializer()

    class Meta:
        model = InstitutionToInstitutionInvitation
        exclude = ['source']


class IncomingInvitationSerializer(serializers.ModelSerializer):
    source = InvitedInstitutionSerializer()

    class Meta:
        model = InstitutionToInstitutionInvitation
        exclude = ['destination']


class _BaseInstitutionSerializer(BaseInstitutionSerializer, EagerLoadingMixin):
    outgoing_invitations = OutgoingInvitationSerializer(many=True, read_only=True)
    incoming_invitations = IncomingInvitationSerializer(many=True, read_only=True)
    partnerships = PartnershipSerializer(many=True, read_only=True)

    SELECT_RELATED_FIELDS = []
    PREFETCH_RELATED_FIELDS = ['emails', 'phones']

    class Meta(BaseInstitutionSerializer.Meta):
        fields = copy(BaseInstitutionSerializer.Meta.fields)
        fields += [
            'outgoing_invitations', 'incoming_invitations', 'partnerships'
        ]


class InstitutionSerializer(_BaseInstitutionSerializer):
    def to_representation(self, instance):
        if isinstance(instance, HighSchool):
            return HighSchoolSerializer(instance, context=self.context).to_representation(instance)
        if isinstance(instance, School):
            return SchoolSerializer(instance, context=self.context).to_representation(instance)
        return super(InstitutionSerializer, self).to_representation(instance)


class SchoolSerializer(_BaseInstitutionSerializer):
    class Meta(_BaseInstitutionSerializer.Meta):
        model = School
        fields = copy(_BaseInstitutionSerializer.Meta.fields)
        fields += [

        ]


class HighSchoolSerializer(_BaseInstitutionSerializer):
    departments = HighSchoolDepartmentSerializer(many=True)

    class Meta(_BaseInstitutionSerializer.Meta):
        model = HighSchool
        fields = copy(_BaseInstitutionSerializer.Meta.fields)
        fields += [
            'departments', 'accreditation'
        ]
