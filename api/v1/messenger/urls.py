from django.conf.urls import url
from .views import (
    MessageDetailView, GroupDetailView, MessagesListView, GroupListView
)


urlpatterns = [
    url(r'^groups/$', GroupListView.as_view(), name='groups'),
    url(r'^groups/(?P<group_pk>[0-9]+)/$', GroupDetailView.as_view(), name='groups'),
    url(r'^groups/(?P<group_pk>[0-9]+)/messages/$', MessagesListView.as_view(), name='messages'),
    url(r'^groups/(?P<group_pk>[0-9]+)/messages/(?P<pk>[0-9]+)/$', MessageDetailView.as_view(), name='messages'),
]
