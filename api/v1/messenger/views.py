from rest_framework import generics
from .serializers import (
    MessageWithNestedSenderSerializer, GroupSerializer
)
from messenger.models import Group
from rest_framework import permissions
from rest_framework import pagination
from .permissions import ProfileInGroupForGroups, ProfileInGroupForMessages


class MessageDetailView(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated, ProfileInGroupForMessages]
    serializer_class = MessageWithNestedSenderSerializer

    def get_queryset(self):
        group = self.get_group()
        return group.messages

    def get_group(self):
        return Group.objects.get(pk=self.kwargs['group_pk'])

    def get_groups_allowed_queryset(self):
        profile = self.request.profile
        return profile.message_groups.filter(active=True)


class MessagesListPagination(pagination.LimitOffsetPagination):
    default_limit = 25


class MessagesListView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated, ProfileInGroupForMessages]
    serializer_class = MessageWithNestedSenderSerializer
    pagination_class = MessagesListPagination

    def get_queryset(self):
        group = self.get_group()
        return group.messages

    def get_group(self):
        return Group.objects.get(pk=self.kwargs['group_pk'])

    def get_groups_allowed_queryset(self):
        profile = self.request.profile
        return profile.message_groups.filter(active=True)


class GroupDetailView(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated, ProfileInGroupForGroups]
    queryset = Group.objects.filter(active=True)
    serializer_class = GroupSerializer
    lookup_url_kwarg = 'group_pk'

    def get_groups_allowed_queryset(self):
        profile = self.request.profile
        return profile.message_groups.filter(active=True)


class GroupListView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = Group.objects.filter(active=True)
    serializer_class = GroupSerializer

    def get_queryset(self):
        profile = self.request.profile
        return profile.message_groups.filter(active=True)
