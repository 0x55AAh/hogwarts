from rest_framework import permissions


class ProfileInGroupForGroups(permissions.BasePermission):
    message = 'Group is not allowed.'

    def has_object_permission(self, request, view, obj):
        return obj in view.get_groups_allowed_queryset()


class ProfileInGroupForMessages(permissions.BasePermission):
    message = 'Group is not allowed.'

    def has_permission(self, request, view):
        group = view.get_group()
        return group in view.get_groups_allowed_queryset()
