from rest_framework import serializers
from messenger.models import (
    ClientMessage, TextMessage, UrlMessage, FileMessage, ImageMessage,
    MessageStatus, Group
)
from core.models import Person


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        exclude = ['polymorphic_ctype']


class TextMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextMessage
        exclude = ['polymorphic_ctype']


class UrlMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = UrlMessage
        exclude = ['polymorphic_ctype']


class FileMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileMessage
        exclude = ['polymorphic_ctype']


class ImageMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageMessage
        exclude = ['polymorphic_ctype']


class TextMessageWithNestedSenderSerializer(serializers.ModelSerializer):
    sender = PersonSerializer()
    status_id = serializers.SerializerMethodField('_get_status_id')

    def _get_status_id(self, obj):
        if 'request' not in self.context:
            return None
        person = self.context['request'].profile
        if obj.sender == person:
            return None
        return obj.statuses.get(person=person).id

    class Meta:
        model = TextMessage
        fields = [
            'sender', 'value', 'quoted', 'draft', 'created', 'updated',
            'institution', 'group', 'status_id', 'active', 'id'
        ]


class UrlMessageWithNestedSenderSerializer(serializers.ModelSerializer):
    sender = PersonSerializer()

    class Meta:
        model = UrlMessage
        exclude = ['polymorphic_ctype']


class FileMessageWithNestedSenderSerializer(serializers.ModelSerializer):
    sender = PersonSerializer()

    class Meta:
        model = FileMessage
        exclude = ['polymorphic_ctype']


class ImageMessageWithNestedSenderSerializer(serializers.ModelSerializer):
    sender = PersonSerializer()

    class Meta:
        model = ImageMessage
        exclude = ['polymorphic_ctype']


class MessageSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        if isinstance(instance, TextMessage):
            return TextMessageSerializer(instance, context=self.context).to_representation(instance)
        elif isinstance(instance, UrlMessage):
            return UrlMessageSerializer(instance, context=self.context).to_representation(instance)
        elif isinstance(instance, FileMessage):
            return FileMessageSerializer(instance, context=self.context).to_representation(instance)
        elif isinstance(instance, ImageMessage):
            return ImageMessageSerializer(instance, context=self.context).to_representation(instance)
        return super(MessageSerializer, self).to_representation(instance)

    class Meta:
        model = ClientMessage
        exclude = ['polymorphic_ctype']


class MessageWithNestedSenderSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        if isinstance(instance, TextMessage):
            return TextMessageWithNestedSenderSerializer(instance, context=self.context).to_representation(instance)
        elif isinstance(instance, UrlMessage):
            return UrlMessageWithNestedSenderSerializer(instance, context=self.context).to_representation(instance)
        elif isinstance(instance, FileMessage):
            return FileMessageWithNestedSenderSerializer(instance, context=self.context).to_representation(instance)
        elif isinstance(instance, ImageMessage):
            return ImageMessageWithNestedSenderSerializer(instance, context=self.context).to_representation(instance)
        return super(MessageWithNestedSenderSerializer, self).to_representation(instance)

    class Meta:
        model = ClientMessage
        exclude = ['polymorphic_ctype']


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'


class MessageStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageStatus
        fields = '__all__'
