from django.conf.urls import url, include
from .institutions import urls as institutions_urls
from .ui import urls as ui_urls
from .messenger import urls as messenger_urls

urlpatterns = [
    url(r'^institutions/', include(institutions_urls, namespace='institutions')),
    url(r'^ui/', include(ui_urls, namespace='ui')),
    url(r'^messenger/', include(messenger_urls, namespace='messenger')),
]
