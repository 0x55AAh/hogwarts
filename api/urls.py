from django.conf.urls import url, include
from graphene_django.views import GraphQLView

urlpatterns = [
    url(r'^', include('api.v1.urls')),  # Default
    url(r'^v1/', include('api.v1.urls', namespace='v1')),
    url(r'^v2/', GraphQLView.as_view(graphiql=True))
]
