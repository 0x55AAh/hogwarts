import graphene
from graphene_django.types import DjangoObjectType
from messenger.models import (
    ClientMessage, Group, TextMessage, UrlMessage, FileMessage, ImageMessage
)
from core.models import Person


class PersonType(DjangoObjectType):
    class Meta:
        model = Person


class MessengerGroupType(DjangoObjectType):
    class Meta:
        model = Group


class Query(graphene.AbstractType):
    persons = graphene.List(PersonType)
    groups = graphene.List(MessengerGroupType)

    def resolve_persons(self, args, context, info):
        return Person.objects.filter(active=True)

    def resolve_groups(self, args, context, info):
        return Group.objects.filter(active=True)
