import graphene
import api.v2.messenger.schema


class Query(
    api.v2.messenger.schema.Query,
    graphene.ObjectType
):
    # This class extends all abstract apps level Queries and graphene.ObjectType
    pass

schema = graphene.Schema(query=Query)
