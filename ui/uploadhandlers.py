from django.core.files.uploadhandler import FileUploadHandler
from django.core.cache import cache
from ipware.ip import get_ip


class UploadProgressHandlerBase(FileUploadHandler):
    """
    Tracks progress for file uploads.
    The http post request must contain a header or query parameter, 'X-Progress-ID'
    which should contain a unique string to identify the upload to be tracked.
    """

    def __init__(self, request=None):
        super(UploadProgressHandlerBase, self).__init__(request)
        self.progress_id = None
        self.storage_key = None

    def init_storage_data(self):
        raise NotImplementedError('subclasses of UploadProgressHandlerBase must provide a init_storage_data() method')

    def update_storage_data(self, raw_data, start):
        raise NotImplementedError('subclasses of UploadProgressHandlerBase must provide a update_storage_data() method')

    def handle_raw_input(self, input_data, META, content_length, boundary, encoding=None):
        self.content_length = content_length
        if 'X-Progress-ID' in self.request.GET:
            self.progress_id = self.request.GET['X-Progress-ID']
        elif 'X-Progress-ID' in self.request.META:
            self.progress_id = self.request.META['X-Progress-ID']
        if self.progress_id:
            self.storage_key = '%s_%s' % (get_ip(self.request), self.progress_id)
            self.init_storage_data()

    def receive_data_chunk(self, raw_data, start):
        if self.storage_key:
            self.update_storage_data(raw_data, start)
        return raw_data

    def file_complete(self, file_size):
        pass

    def upload_complete(self):
        pass


class UploadProgressCacheHandler(UploadProgressHandlerBase):
    def init_storage_data(self):
        cache.set(self.storage_key, {
            'length': self.content_length,
            'uploaded': 0,
            'completed': False
        })

    def update_storage_data(self, raw_data, start):
        data = cache.get(self.storage_key, {})
        data['uploaded'] += len(raw_data)
        cache.set(self.storage_key, data)

    def file_complete(self, file_size):
        data = cache.get(self.storage_key, {})
        data['completed'] = True
        cache.set(self.storage_key, data)
