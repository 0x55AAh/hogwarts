from django import template
import uuid

from sorl.thumbnail.templatetags.thumbnail import ThumbnailNodeBase
from sorl.thumbnail.images import DummyImageFile

register = template.Library()


@register.inclusion_tag('tags/pagination.html', takes_context=True)
def pagination(context, page, begin_pages=1, end_pages=1,
               before_pages=2, after_pages=2):
    """
    Return a Digg-like pagination,
    by splitting long list of page into 3 blocks of pages.
    """
    GET_string = ''
    for key, value in context['request'].GET.items():
        if key != 'page':
            GET_string += '&%s=%s' % (key, value)

    page_range = list(page.paginator.page_range)
    begin = page_range[:begin_pages]
    end = page_range[-end_pages:]
    middle = page_range[max(page.number - before_pages - 1, 0):
    page.number + after_pages]

    if set(begin) & set(middle):  # [1, 2, 3], [2, 3, 4], [...]
        begin = sorted(set(begin + middle))  # [1, 2, 3, 4]
        middle = []
    elif begin[-1] + 1 == middle[0]:  # [1, 2, 3], [4, 5, 6], [...]
        begin += middle  # [1, 2, 3, 4, 5, 6]
        middle = []
    elif middle[-1] + 1 == end[0]:  # [...], [15, 16, 17], [18, 19, 20]
        end = middle + end  # [15, 16, 17, 18, 19, 20]
        middle = []
    elif set(middle) & set(end):  # [...], [17, 18, 19], [18, 19, 20]
        end = sorted(set(middle + end))  # [17, 18, 19, 20]
        middle = []

    if set(begin) & set(end):  # [1, 2, 3], [...], [2, 3, 4]
        begin = sorted(set(begin + end))  # [1, 2, 3, 4]
        middle, end = [], []
    elif begin[-1] + 1 == end[0]:  # [1, 2, 3], [...], [4, 5, 6]
        begin += end  # [1, 2, 3, 4, 5, 6]
        middle, end = [], []

    return {
        'template': template,
        'page': page,
        'begin': begin,
        'middle': middle,
        'end': end,
        'GET_string': GET_string
    }


@register.inclusion_tag('tags/progress-bar.html', takes_context=True)
def progress_bar(context, theme):
    return {
        'progress_id': uuid.uuid4(),
        'progress_theme': theme
    }


@register.inclusion_tag('tags/footer.html', takes_context=True)
def footer(context, classes=''):
    from hogwarts import __version__
    return {
        'classes': classes,
        'version': __version__
    }


class ThumbnailNode(ThumbnailNodeBase):
    child_nodelists = ('nodelist_file', 'nodelist_empty')
    error_msg = 'Syntax error. Expected: ``thumbnail_ui obj thumbnail_name as var``'

    def __init__(self, parser, token):
        bits = token.split_contents()

        self.obj = parser.compile_filter(bits[1])
        self.thumbnail_name = parser.compile_filter(bits[2])

        self.as_var = None
        self.nodelist_file = None

        if bits[-2] == 'as':
            self.as_var = bits[-1]
            self.nodelist_file = parser.parse(('empty', 'endthumbnail_ui',))
            if parser.next_token().contents == 'empty':
                self.nodelist_empty = parser.parse(('endthumbnail_ui',))
                parser.delete_first_token()

    def _render(self, context):
        obj = self.obj.resolve(context)
        thumbnail_name = self.thumbnail_name.resolve(context)

        obj_meta = getattr(obj, '_meta', None)
        if obj_meta:
            thumbnail = obj_meta.get_field(thumbnail_name).get_thumbnail(obj)
        else:
            thumbnail = None

        if not thumbnail or (isinstance(thumbnail, DummyImageFile) and self.nodelist_empty):
            if self.nodelist_empty:
                return self.nodelist_empty.render(context)
            else:
                return ''

        if self.as_var:
            context.push()
            context[self.as_var] = thumbnail
            output = self.nodelist_file.render(context)
            context.pop()
        else:
            output = thumbnail.url

        return output

    def __repr__(self):
        return "<ThumbnailNode>"

    def __iter__(self):
        for node in self.nodelist_file:
            yield node
        for node in self.nodelist_empty:
            yield node


@register.tag
def thumbnail_ui(parser, token):
    return ThumbnailNode(parser, token)
