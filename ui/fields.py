import json
from jsonfield import JSONField
from jsonfield.fields import JSONFormField
from sorl.thumbnail import get_thumbnail
from django.utils.translation import ugettext_lazy as _
from django.forms.utils import ValidationError
try:
    from django.utils import six
except ImportError:
    import six


class ThumbnailField(JSONField):
    description = _('Thumbnail')

    def __init__(self, image_field, size, *args, **kwargs):
        if '__' in image_field:
            self.image_field, self.image_fk_field = image_field.split('__')
        else:
            self.image_field, self.image_fk_field = image_field, None
        self.width, self.height = size
        self.size = '%dx%d' % (self.width, self.height)
        self.options = kwargs.pop('options', {'crop': 'center'})
        if 'default' not in kwargs:
            kwargs['default'] = self.options
        else:
            kwargs['default'].update(self.options)
        kwargs.update({'editable': True, 'null': True})
        super(ThumbnailField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super(ThumbnailField, self).deconstruct()
        if self.image_fk_field:
            image_field = '%s__%s' % (self.image_field, self.image_fk_field)
        else:
            image_field = self.image_field
        args += (image_field, (self.width, self.height))
        kwargs.update({'options': self.options})
        del kwargs['null']
        return name, path, args, kwargs

    def to_python(self, value):
        if isinstance(value, six.string_types):
            try:
                return json.loads(value, **self.load_kwargs)
            except ValueError:
                raise ValidationError(_('Enter valid JSON'))
        return value

    def from_db_value(self, value, expression, connection, context):
        return self.to_python(value)

    def get_source(self, obj):
        return getattr(obj, self.image_field)

    def get_thumbnail(self, obj):
        image = self.get_source(obj)
        if self.image_fk_field and image:  # image is ForeignKey
            image = getattr(image, self.image_fk_field)
        if image:
            value = self._get_val_from_obj(obj)
            if isinstance(value, dict):
                options = value
            else:
                options = json.loads(value, **self.load_kwargs)
            thumbnail = get_thumbnail(image, self.size, **options)
            return thumbnail


class ThumbnailFormField(JSONFormField):
    pass
