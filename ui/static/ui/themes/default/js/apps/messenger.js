$(function () {
    const systemAction = {
        PING: "ping",
        WARNING: "warning",
        ERROR: "error",
        MUTED: "muted",
        UN_MUTED: "un_muted",
        SUBSCRIBE: "subscribe",
        LEAVE: "leave",
        TYPING: "typing",
        STOP_TYPING: "stop_typing",
        SENDING_FILE: "sending_file",
        SENDING_IMAGE: "sending_image",
        STOP_SENDING_FILE: "stop_sending_file",
        STOP_SENDING_IMAGE: "stop_sending_image",
        READ: "read"
    };

    const CREATE = "create",
          UPDATE = "update",
          DELETE = "delete",
          PATCH = "patch";

    function ws_connection_start() {
        // TODO: set connecting process indication
    }
    function ws_connection_opened() {
        // TODO: remove connecting process indication
        console.log("Connected to messenger socket");
    }


    const session_key = $("body").data("session-key");
    const profile_id = $('body').data('profile-id');
    const ws_bridge = new channels.WebSocketBridge({
        maxReconnectionDelay: 10000,
        minReconnectionDelay: 1500,
        reconnectionDelayGrowFactor: 1.3,
        connectionTimeout: 4000,
        maxRetries: Infinity,
        debug: true
    });
    var ms = new MessageSenderWithRetry(ws_bridge);

    ws_connection_start();
    ws_bridge.connect("/message/?session_key=" + session_key);
    ws_bridge.listen();

    ws_bridge.socket.addEventListener("open", function() {
        ws_connection_opened();

        // Auto resend messages after reconnect
        ms.resendAll();
    });
    ws_bridge.socket.addEventListener("close", function() {
        console.log("Disconnected from messenger socket");
    });
    ws_bridge.socket.addEventListener("error", function(event) {
        console.error("Messenger error occurred: " + event.message);
    });

    var myMessageTemplate = '<li class="media reversed message" data-message-id="{message_id}" data-stream="{stream}" style="display: none">\
                        <div class="media-body">\
                            <div class="media-content bg-info">\
                                <div class="media-heading">\
                                    <span class="text-semibold">{sender_name}</span>\
                                </div>\
                                <div class="message-text">{message_text}</div>\
                            </div>\
                            <span class="media-annotation display-block mt-10">\
                                <span class="message-date">{date}</span>\
                                <span class="message-status"></span>\
                            </span>\
                        </div>\
                        <div class="media-right">\
                            <a href="#">\
                                <img src="{photo}" class="img-circle img-xs" alt="">\
                            </a>\
                        </div>\
                    </li>';

    var otherMessageTemplate = '<li class="media message" data-status-id="{status_id}" data-stream="{stream}" style="display: none">\
                        <div class="media-left">\
                            <a href="#">\
                                <img src="{photo}" class="img-circle img-xs" alt="">\
                            </a>\
                        </div>\
                        <div class="media-body">\
                            <div class="media-content bg-info">\
                                <div class="media-heading">\
                                    <span class="text-semibold">{sender_name}</span>\
                                </div>\
                                <div class="message-text">{message_text}</div>\
                            </div>\
                            <span class="media-annotation display-block mt-10">{date}</span>\
                        </div>\
                    </li>';

    var typingMessageTemplate = '<li class="media message typing" style="display: none">\
                        <div class="media-left">\
                            <a href="#">\
                                <img src="{photo}" class="img-circle img-xs" alt="">\
                            </a>\
                        </div>\
                        <div class="media-body">\
                            <div class="media-heading">\
                                <span class="text-semibold">{sender_name}</span>\
                            </div>\
                            <span class="media-annotation display-block mt-10" style="margin-top: 0!important;">Typing...</span>\
                        </div>\
                    </li>';

    function getMyMessage(o) {
        return myMessageTemplate.format(o);
    }
    function getOtherMessage(o) {
        return otherMessageTemplate.format(o);
    }
    function getTypingMessage(o) {
        return typingMessageTemplate.format(o);
    }

    var Guid = (function() {
        var Guid = {};
        Guid.newGuid = function() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        };
        return Guid;
    }());

    var message_processing = {
        onTextMessageReceived: function (message) {
            this.onTextStopTyping(message.sender, message.group, false);
            var $message = $(getOtherMessage({
                // photo: message.sender.photo,
                photo: "/static/ui/themes/default/images/placeholder.jpg",
                sender_name: message.sender.last_name + " " + message.sender.first_name,
                date: moment(message.created).format('MMM D YYYY, [at] h:mm a'),
                message_text: message.value
            }));
            message_animate($message, "fadeInUp");
            ion.sound.play("incoming_message");
        },
        onTextMessageUpdateResponseReceived: function (action, create) {
            console.log(action);
            var $form = $("#send-message-form"), message = action.data;
            if (message.draft) {
                if (action.errors.length === 0) {
                    $form.attr("data-draft-id", action.data.id);
                    // $form.find("[name=text_message]").text(action.data.value);
                }
            } else {
                var $message = $(".message[data-request-id=" + action.request_id + "]");
                if (action.errors.length === 0) {
                    $message.removeAttr("data-request-id").removeAttr("data-action")
                        .attr("data-message-id", action.data.id);
                    setMessageStatus($message, "sent");
                } else {
                    setMessageStatus($message, "failed");
                }
            }
            ms.cleanQueue(action.request_id);
        },
        onTextMessageDeleteResponseReceived: function (action) {
            if (action.errors.length === 0) {
                $('.chat-list li[data-message-id=' + action.data.id + ']').remove();
                console.log('Deleted successfully!');
            } else {
                console.log(action);
            }
        },
        textTypingSenders: {},
        textTypingCount: function () {
            return Object.keys(this.textTypingSenders).length;
        },
        onTextTyping: function (sender, group) {
            var $message = $(getTypingMessage({
                photo: "/static/ui/themes/default/images/placeholder.jpg",
                sender_name: sender.last_name + " " + sender.first_name
            }));
            $message.attr("data-sender", sender.id);
            message_animate($message, "fadeInUp");

            var $group_tab = $(".massage-group-tab[data-group-id=" + group + "]");
            $group_tab.find(".massage-group-tab__bottom-left").hide(0);
            $group_tab.find(".massage-group-tab__bottom-left__typing-status")
                .text(sender.first_name + ": typing...").show(0);

            this.textTypingSenders[sender.id] = sender;
        },
        onTextStopTyping: function (sender, group, animate) {
            var $message = $(".chat-list > li.media.typing[data-sender=" + sender.id + "]");
            if (animate) {
                message_animate($message, "fadeOutDown", function () {
                    $message.remove();
                });
            } else {
                $message.remove();
            }

            var $group_tab = $(".massage-group-tab[data-group-id=" + group + "]");
            $group_tab.find(".massage-group-tab__bottom-left").hide(0);
            $group_tab.find(".massage-group-tab__bottom-left__last-message").show(0);

            delete this.textTypingSenders[sender.id];
        },
        onSubscribe: function(sender, group) {
            console.log(sender, group, 'subscribe');
        },
        onLeave: function(sender, group) {
            console.log(sender, group, 'leave');
        },
        onFileSending: function(sender, group) {
            console.log(sender, group, 'file sending');
        },
        onFileStopSending: function(sender, group) {
            console.log(sender, group, 'stop file sending');
        },
        onImageSending: function(sender, group) {
            console.log(sender, group, 'image sending');
        },
        onImageStopSending: function(sender, group) {
            console.log(sender, group, 'stop image sending');
        },
        onMuted: function(sender, group) {
            console.log(sender, group, 'muted');
        },
        onUnMuted: function(sender, group) {
            console.log(sender, group, 'un muted');
        },
        onPing: function(sender, group, text) {
            console.log(sender, group, text, 'ping');
        },
        onError: function(sender, group, text) {
            console.log(sender, group, text, 'error');
        },
        onWarning: function(sender, group, text) {
            console.log(sender, group, text, 'warning');
        },
        onRead: function(sender, group) {
            console.log(sender, group, 'read');
        }
    };

    ws_bridge.demultiplex("text-message", function(action, stream) {
        var message = action.data, sender_id = action.sender, group_id = action.group;
        switch (action.action) {
            case "create":
                if ("response_status" in action) {
                    message_processing.onTextMessageUpdateResponseReceived(action, true);
                } else if (sender_id !== profile_id) {
                    message_processing.onTextMessageReceived(message);
                }
                break;
            case "update":
                if ("response_status" in action) {
                    message_processing.onTextMessageUpdateResponseReceived(action, false);
                } else if (sender_id !== profile_id) {
                    message_processing.onTextMessageReceived(message);
                }
                break;
            case "delete":
                if ("response_status" in action) {
                    message_processing.onTextMessageDeleteResponseReceived(action);
                } else if (sender_id !== profile_id) {
                    console.log('Deleted!');
                }
                break;
            default:
                break;
        }
    });
    ws_bridge.demultiplex("url-message", function(action, stream) {
        console.log(action, stream);
    });
    ws_bridge.demultiplex("file-message", function(action, stream) {
        console.log(action, stream);
    });
    ws_bridge.demultiplex("image-message", function(action, stream) {
        console.log(action, stream);
    });

    ws_bridge.demultiplex("profile-network-status", function(action, stream) {
        console.log(action, stream);
    });
    ws_bridge.demultiplex("system-message", function(action, stream) {
        var sender = action.sender, group = action.group, text = action.text;
        switch(action.action) {
            case systemAction.PING:
                message_processing.onPing(sender, group, text);
                break;
            case systemAction.ERROR:
                message_processing.onError(sender, group, text);
                break;
            case systemAction.WARNING:
                message_processing.onWarning(sender, group, text);
                break;
            case systemAction.TYPING:
                if (sender.id !== profile_id) {
                    message_processing.onTextTyping(sender, group);
                }
                break;
            case systemAction.STOP_TYPING:
                if (sender.id !== profile_id) {
                    message_processing.onTextStopTyping(sender, group, true);
                }
                break;
            case systemAction.SENDING_FILE:
                message_processing.onFileSending(sender, group);
                break;
            case systemAction.STOP_SENDING_FILE:
                message_processing.onFileStopSending(sender, group);
                break;
            case systemAction.SENDING_IMAGE:
                message_processing.onImageSending(sender, group);
                break;
            case systemAction.STOP_SENDING_IMAGE:
                message_processing.onImageStopSending(sender, group);
                break;
            case systemAction.ONLINE:
                message_processing.onOnline(sender, group);
                break;
            case systemAction.OFFLINE:
                message_processing.onOffline(sender, group);
                break;
            case systemAction.SUBSCRIBE:
                message_processing.onSubscribe(sender, group);
                break;
            case systemAction.LEAVE:
                message_processing.onLeave(sender, group);
                break;
            case systemAction.MUTED:
                message_processing.onMuted(sender, group);
                break;
            case systemAction.UN_MUTED:
                message_processing.onUnMuted(sender, group);
                break;
            case systemAction.READ:
                message_processing.onRead(sender, group);
                break;
        }
    });
    ws_bridge.demultiplex("message-status", function(action, stream) {
        console.log(action, stream);
    });
    ws_bridge.demultiplex("group-membership", function(action, stream) {
        console.log(action, stream);
    });

    ion.sound({
        sounds: [
            {
                name: "button_tiny",
                alias: "incoming_message"
            }
        ],
        path: "/static/ui/themes/default/sounds/",
        preload: true,
        volume: 1.0
    });

    function message_animate($message, animation, callback) {
        var $messages = $('.chat-list');
        $messages.append($message.show(0)).scrollTop($messages[0].scrollHeight);
        $message.addClass("animated " + animation)
            .one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                $message.removeClass("animated " + animation);
                if (typeof callback === "function")
                    callback();
            });
    }

    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function MessageSenderWithRetry(ws_bridge) {
        this.socketConnectionRetries = 0;
        this.socketConnectionRetriesMax = 10;
        this.ws_bridge = ws_bridge;
        this.waitForSocketConnection = function(send, error) {
            var self = this;
            setTimeout(function() {
                if (self._isConnectionOpen()) {
                    if(send !== null) {
                        send();
                    }
                } else {
                    if (self.socketConnectionRetries >= self.socketConnectionRetriesMax) {
                        if(error !== null) {
                            error();
                        }
                        return;
                    }
                    self.socketConnectionRetries++;
                    self.waitForSocketConnection(send, error);
                }
            }, 1000);
        };
        this.queue = {};
        this._isConnectionOpen = function () {
            return this.ws_bridge.socket.readyState === this.ws_bridge.socket.OPEN;
        };
        this._send = function (message, stream) {
            this.ws_bridge.stream(stream).send(message);
        };
        this.send = function (message, stream, $message) {
            this.queue[message.request_id] = {
                message: message,
                stream: stream,
                $message: $message
            };
            // setMessageStatus($message, "sending");
            if (this._isConnectionOpen()) {
                this._send(message, stream);
            } else {
                this.waitForSocketConnection(
                    function () {
                        this._send(message, stream);
                    },
                    function () {
                        if ($message !== null) {
                            setMessageStatus($message, "failed");
                        }
                    }
                );
            }
            this.socketConnectionRetries = 0;
        };
        this.resend = function (request_id) {
            var queueEntry = this.queue[request_id],
                message = queueEntry.message,
                stream = queueEntry.stream,
                $message = queueEntry.$message;
            this.send(message, stream, $message);
        };
        this.cleanQueue = function (request_id) {
            if (request_id in this.queue) {
                delete this.queue[request_id];
                return true;
            }
            return false;
        };
        this.resendAll = function () {
            var self = this;
            $.each(this.queue, function (request_id) {
                self.resend(request_id);
            });
        }
    }

    function setMessageStatus($message, status) {
        var $status = $message.find(".message-status");
        switch (status) {
            case "sending":
                $status.html('<i class="icon-spinner9 spinner position-right text-muted"></i>');
                break;
            case "sent":
                $status.html('<i class="icon-circle2 position-right text-muted"></i>');
                break;
            case "read":
                $status.html('<i class="icon-checkmark-circle position-right text-muted"></i>');
                break;
            case "failed":
                $status.html('<i class="icon-cancel-circle2 position-right text-warning"></i>');
                break;
        }
    }

    $("#send-message-form [name=text_message]").on("keydown", function (event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            $("#send-message-form").trigger('submit');
        }
    });
    $("#send-message-form").on("focusout", function (event) {
        var $form = $(this);
        setTimeout(function () {
            if (!$form.find(':focus').length) {
                $form.trigger('submit', [event.type]);
            }
        }, 0);
    });

    // Send text message
    $("#send-message-form").on("submit", function (event, parentEvent) {
        event.preventDefault();
        var form = this, $form = $(this), data = $(this).serializeObject();
        if ((/^\s*$/).test(data.text_message))
            return;

        var draftID = $form.data("draft-id"),
            request_id = Guid.newGuid(),
            action = (draftID === undefined) ? CREATE : UPDATE,
            draft, $message, group = 1, stream = "text-message";

        if (parentEvent !== 'focusout') {
            draft = false;
            $message = $(getMyMessage({
                photo: "/static/ui/themes/default/images/placeholder.jpg",
                sender_name: "James Alexander",
                date: moment().format('MMM D YYYY, [at] h:mm a'),
                message_text: data.text_message
            }));
            $message.attr("data-request-id", request_id)
                .attr("data-action", action)
                .find(".message-status");
            setMessageStatus($message, "sending");
            message_animate($message, "fadeInUp");
            $form.removeAttr("data-draft-id");
            $form.removeData("draft-id");
        } else {
            draft = true;
            $message = null;
        }

        var message = {
                action: action,
                data: {
                    value: data.text_message,
                    draft: draft,
                    group: group
                },
                group: group,
                request_id: request_id
            };
        if (action === UPDATE) {
            message.pk = draftID;
        }

        ms.send(message, stream, $message);

        if (!draft) {
            form.reset();
            $form.find("[name=text_message]").val(null);
        }
    });

    // Keyboard typing listener
    var timer;
    $("#send-message-form [name=text_message]").on("keyup keydown", function (event) {
        if (timer) {
            clearTimeout(timer);
        } else {
            ws_bridge.stream("system-message").send({
                action: systemAction.TYPING,
                group: 1
            });
        }
        timer = setTimeout(function() {
            timer = 0;
            ws_bridge.stream("system-message").send({
                action: systemAction.STOP_TYPING,
                group: 1
            });
        }, 3000);
    });

    function create_messages_sheet(messages) {
        var _messages = "";
        $.each(messages, function () {
            var o = {
                // photo: this.sender.photo,
                photo: "/static/ui/themes/default/images/placeholder.jpg",
                sender_name: this.sender.last_name + " " + this.sender.first_name,
                date: moment(this.created).format('MMM D YYYY, [at] h:mm a'),
                message_text: this.value,
                message_id: this.id
            };
            if (this.sender.id === profile_id) {
                _messages = getMyMessage(o) + _messages;
            } else {
                o.status_id = this.status_id;
                _messages = getOtherMessage(o) + _messages;
            }
        });
        return _messages;
    }

    // Select group and initial load group messages
    $(".sidebar-category li.media").on("click", function(event) {
        var group_id = $(this).data("group-id"),
            groupTab = this,
            $history = $('.chat-list');
        if (!$(this).hasClass('active')) {
            $.ajax({
                url: "/api/messenger/groups/" + group_id,
                dataType: "json",
                success: function(group) {
                    $(".panel-heading .media-body span.text-semibold").text(group.name);
                    $.ajax({
                        url: "/api/messenger/groups/" + group_id + "/messages/",
                        dataType: "json",
                        beforeSend: function() {
                            $(groupTab).closest("ul").find("li").removeClass("active");
                            $(groupTab).addClass("active");
                            history.replaceState(null, "", "?group=" + group_id);
                            // Remove text message
                            // ws_bridge.stream("text-message").send({
                            //     action: "delete",
                            //     pk: 529,
                            //     data: {
                            //         group: 1
                            //     },
                            //     group: 1,
                            //     request_id: Guid.newGuid()
                            // });
                        },
                        success: function(data) {
                            var new_messages = create_messages_sheet(data.results);
                            $history.data("api-pagination-next", data.next).html(new_messages);
                            $history.find("li:hidden").fadeIn("fast");
                            $history.scrollTop($history[0].scrollHeight);
                            // Set read status
                            // ws_bridge.stream("message-status").send({
                            //     action: "patch",
                            //     pk: 310,
                            //     data: {
                            //         value: "read",
                            //     },
                            //     group: 1
                            // });
                        },
                        error: function(jqXHR, textStatus, errorThrown) {

                        },
                        complete: function(jqXHR, textStatus) {

                        }
                    });
                }
            });
        } else {

        }
    });

    //
    var updating_history = false;
    $(".chat-list").on("scroll", function(event) {
        var $history = $(this);
        if ($history.scrollTop() < $history.height() * 3 && !updating_history) {
            var url = $history.data("api-pagination-next");
            if (typeof(url) === "undefined" || url === null) return;
            $.ajax({
                url: url,
                dataType: "json",
                beforeSend: function() {
                    updating_history = true;
                },
                success: function(data) {
                    var new_messages = create_messages_sheet(data.results);
                    $history.data("api-pagination-next", data.next);
                    $history.prepend(new_messages).find("li:hidden").fadeIn("fast");
                },
                error: function(jqXHR, textStatus, errorThrown) {

                },
                complete: function(jqXHR, textStatus) {
                    updating_history = false;
                    if ($history.scrollTop() < $history.height() * 3)
                        $history.trigger("scroll");
                }
            });
        }
    });

});
