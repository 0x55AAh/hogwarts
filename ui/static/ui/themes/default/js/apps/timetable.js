$(function() {

    var events = [
        {
            title: 'All Day Event',
            start: '2014-11-01',
            color: '#EF5350'
        },
        {
            title: 'Long Event',
            start: '2014-11-07',
            end: '2014-11-10',
            color: '#DCEDC8',
            rendering: 'background'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-06T16:00:00',
            color: '#26A69A'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: '2014-11-16T16:00:00'
        },
        {
            title: 'Conference',
            start: '2014-11-11',
            end: '2014-11-13'
        },
        {
            title: 'Meeting',
            start: '2014-11-12T10:30:00',
            end: '2014-11-12T12:30:00'
        },
        {
            title: 'Lunch',
            start: '2014-11-12T12:00:00'
        },
        {
            title: 'Happy Hour',
            start: '2014-11-12T17:30:00'
        },
        {
            title: 'Dinner',
            start: '2014-11-24T20:00:00'
        },
        {
            title: 'Meeting',
            start: '2014-11-03T10:00:00',
            color: '#5C6BC0'
        },
        {
            title: 'Birthday Party',
            start: '2014-11-13T07:00:00'
        },
        {
            title: 'Vacation',
            start: '2014-11-27',
            end: '2014-11-30',
            color: '#FFCCBC',
            rendering: 'background'
        }
    ];

    var palette = [
        ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
        ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
        ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
        ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
        ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
        ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
        ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
        ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
    ];

    $('.timetable').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: '2014-11-12',
        editable: true,
        droppable: true,
        events: events,
        drop: function() {

        }
    });

    // Initialize the external events
    $('#external-events .fc-event').each(function() {

        // Different colors for events
        $(this).css({'backgroundColor': $(this).data('color'), 'borderColor': $(this).data('color')});

        // Store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).html()), // use the element's text as the event title
            color: $(this).data('color'),
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // Make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0 // original position after the drag
        });
    });

    // Remote data
    $("#ac-remote, #ac-remote2").autocomplete({
        minLength: 2,
        source: "/static/demo_data/jquery_ui/autocomplete.php",
        search: function() {
            $(this).parent().addClass('ui-autocomplete-processing');
        },
        open: function() {
            $(this).parent().removeClass('ui-autocomplete-processing');
        }
    });

    // Multiple messages
    $('#multiple-messages').on('click', function() {
        var message = $(this).next('.blockui-message');
        var block = $(this).parent();
        $(block).block({
            message: message,
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
            	width: 100,
                '-webkit-border-radius': 2,
                '-moz-border-radius': 2,
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            },
            onBlock: function(){
           		clearTimeout();
           	}
        });

        window.setTimeout(function () {
           message.html('<i class="icon-spinner4 spinner"></i> <span class="text-semibold display-block">Loading</span>')
        }, 0);

        window.setTimeout(function () {
           message.html('<i class="icon-spinner4 spinner"></i> <span class="text-semibold display-block">Please wait</span>')
        }, 2000);

        window.setTimeout(function () {
           message.addClass('bg-danger').html('<i class="icon-warning"></i> <span class="text-semibold display-block">Load error</span>')
        }, 4000);

        window.setTimeout(function () {
           $(block).unblock({
           	onUnblock: function(){
           		message.removeClass('bg-danger');
           	}
           });
        }, 6000);
    });

    $(".colorpicker").spectrum({
    	showPalette: true,
        showPaletteOnly: true,
        palette: palette
    });

});