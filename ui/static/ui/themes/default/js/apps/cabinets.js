$(function() {

    $('.cabinet-process').on('click', function() {
        var block = $(this).closest('.panel.panel-body');
        var message = block.find('.blockui-message');
        $(block).block({
            message: message,
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
            	width: 100,
                '-webkit-border-radius': 2,
                '-moz-border-radius': 2,
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            },
            onBlock: function(){
           		clearTimeout();
           	}
        });

        window.setTimeout(function () {
           message.html('<i class="icon-spinner4 spinner"></i> <span class="text-semibold display-block">Loading</span>')
        }, 0);

        window.setTimeout(function () {
           message.html('<i class="icon-spinner4 spinner"></i> <span class="text-semibold display-block">Please wait</span>')
        }, 2000);

        window.setTimeout(function () {
           message.addClass('bg-danger').html('<i class="icon-warning"></i> <span class="text-semibold display-block">Load error</span>')
        }, 4000);

        window.setTimeout(function () {
           $(block).unblock({
           	onUnblock: function(){
           		message.removeClass('bg-danger');
           	}
           });
        }, 6000);
    });

    // Display time picker
    $('.daterange-time').daterangepicker({
        timePicker: true,
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
            format: 'MM/DD/YYYY h:mm a'
        }
    });

    $(".media-body .media-heading").dotdotdot({
		height: 40,
        wrap: 'letter',
        watch: true
	});
    $(".media-body .text-muted").dotdotdot({
		height: 30,
        wrap: 'letter',
        watch: true
	});

     // Remote data
    $("#ac-remote3").autocomplete({
        minLength: 2,
        source: "/static/demo_data/jquery_ui/autocomplete.php",
        search: function() {
            $(this).parent().addClass('ui-autocomplete-processing');
        },
        open: function() {
            $(this).parent().removeClass('ui-autocomplete-processing');
        }
    });

});