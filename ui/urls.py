from django.conf.urls import url, include
from ui.views import UploadProgressCreateView


upload_progress_bar = [
    url(r'^create/$', UploadProgressCreateView.as_view(), name='upload-progress-create'),
]

urlpatterns = [
    url(r'^upload-progress/', include(upload_progress_bar)),
]
