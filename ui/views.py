from django.views.generic import TemplateView
from registration.backends.hmac.views import RegistrationView
from core.models import EducationalInstitution
from core.forms import InstitutionRegistrationForm

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http.response import JsonResponse
from django.template.loader import render_to_string
from django.views.generic import View
from django.conf import settings
import uuid

from messenger.models import Group


class UploadProgressCreateView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if not settings.DEBUG and not self.request.is_ajax():
            raise PermissionDenied('Request must be ajax')
        progress_theme = self.request.GET.get('progress-theme')
        if progress_theme:
            progress_bar = render_to_string(
                'tags/progress-bar.html', {
                    'progress_id': uuid.uuid4(),
                    'progress_theme': progress_theme
                }
            )
            return JsonResponse({'progress_bar': progress_bar})
        return JsonResponse({
            'error': 'You must provide progress-theme query param.'
        })


class InstitutionRegistrationView(RegistrationView):
    template_name = 'registration/registration_form.html'
    email_body_template = 'registration/activation_email.txt'
    email_subject_template = 'registration/activation_email_subject.txt'
    form_class = InstitutionRegistrationForm

    def create_inactive_user(self, form):
        """
        Create the inactive user account and send an email containing
        activation instructions.

        """
        new_user = form.save(commit=False)
        new_user.is_active = False
        new_user.save()

        EducationalInstitution.create(
            model_name=form.get_institution_model_name(),
            user=new_user
        )

        self.send_activation_email(new_user)

        return new_user


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        return context


class TimetableView(LoginRequiredMixin, TemplateView):
    template_name = 'timetable.html'

    def get_context_data(self, **kwargs):
        context = super(TimetableView, self).get_context_data(**kwargs)
        return context


class CabinetsView(LoginRequiredMixin, TemplateView):
    template_name = 'cabinets.html'

    def get_context_data(self, **kwargs):
        context = super(CabinetsView, self).get_context_data(**kwargs)
        return context


class PeoplesView(LoginRequiredMixin, TemplateView):
    template_name = 'peoples.html'

    def get_context_data(self, **kwargs):
        context = super(PeoplesView, self).get_context_data(**kwargs)
        return context


class PartnersView(LoginRequiredMixin, TemplateView):
    template_name = 'partners.html'

    def get_context_data(self, **kwargs):
        context = super(PartnersView, self).get_context_data(**kwargs)
        return context


class SettingsView(LoginRequiredMixin, TemplateView):
    template_name = 'settings.html'

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        return context


class MessagesView(LoginRequiredMixin, TemplateView):
    template_name = 'messages.html'

    def get_context_data(self, **kwargs):
        context = super(MessagesView, self).get_context_data(**kwargs)
        current_group = Group.get_group_by_gid(gid=self.request.GET.get('group'), request=self.request)
        if current_group:
            membership = current_group.memberships.get(person=self.request.profile)
            context['draft'] = membership.draft_messages.first()
        context['current_group'] = current_group
        return context
