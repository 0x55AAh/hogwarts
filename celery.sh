#!/usr/bin/env bash

PROJECT="hogwarts"
LOGLEVEL="info"
INCLUDE="messenger.bindings"
PIDFILE="celery-beat.pid"
LOGFILE="celery-beat.log"
SCHEDULE_FILENAME="celery-beat-schedule.db"

celery worker -B \
-A ${PROJECT} \
-l ${LOGLEVEL} \
-I ${INCLUDE} \
-s ${SCHEDULE_FILENAME}
#--pidfile PIDFILE=${PIDFILE} \
#--logfile LOGFILE=${LOGFILE}