from django.utils.version import get_version

VERSION = (0, 0, 0, 'alpha', 0)

version = __version__ = get_version(VERSION)

default_app_config = 'authentication.apps.AuthenticationConfig'
