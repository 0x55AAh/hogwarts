"""hogwarts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from ui.views import (
    InstitutionRegistrationView, HomeView, SettingsView, TimetableView,
    CabinetsView, PeoplesView, PartnersView, MessagesView
)

urlpatterns = [
    url(r'^', include('authentication.urls')),
    url(r'^register/$', InstitutionRegistrationView.as_view(), name='registration_register'),
    url(r'^', include('registration.backends.hmac.urls')),
    url(r'^api/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include('api.urls', namespace='api')),
    url(r'^ui/', include('ui.urls', namespace='ui')),
]

# Application pages
pages = [
    url(r'^$', HomeView.as_view(), {'slug': 'home'}, name='home'),
    url(r'^settings/$', SettingsView.as_view(), {'slug': 'settings'}, name='settings'),
    url(r'^timetable/$', TimetableView.as_view(), {'slug': 'timetable'}, name='timetable'),
    url(r'^cabinets/$', CabinetsView.as_view(), {'slug': 'cabinets'}, name='cabinets'),
    url(r'^peoples/$', PeoplesView.as_view(), {'slug': 'peoples'}, name='peoples'),
    url(r'^partners/$', PartnersView.as_view(), {'slug': 'partners'}, name='partners'),
    url(r'^messages/$', MessagesView.as_view(), {'slug': 'messages'}, name='messages'),
]

# Application pages
urlpatterns += [
    url(r'^', include(pages, namespace='page')),
]

if settings.DEBUG:
    from django.contrib import admin
    urlpatterns += [
        url(r'^jet/', include('jet.urls', 'jet')),
        url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
        url(r'^channels-api/', include('channels_api.urls')),
        url(r'^admin/', admin.site.urls)
    ]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls))
    ]

if settings.DEBUG:
    from django.views.static import serve
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT})
    ]
