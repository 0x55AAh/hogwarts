import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = TEMPLATE_DEBUG = True
TIME_ZONE = 'Europe/Kiev'

UI_THEME = 'default'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'ui', 'templates'),
            os.path.join(BASE_DIR, 'ui', 'templates', 'ui', 'themes', UI_THEME)
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'core.context_processors.core'
            ],
        },
    },
]

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'ui', 'static', 'ui', 'themes', UI_THEME)
]

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'asgiref.inmemory.ChannelLayer',
        'ROUTING': 'messenger.routing.channel_routing',
    },
}

SITE_HOST = 'localhost:8000'
SITE_URL = 'http://localhost:8000/'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

EMAIL_HOST = 'localhost'
