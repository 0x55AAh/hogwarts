from __future__ import absolute_import

import os
from celery import Celery
import django
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hogwarts.settings')
django.setup()


app = Celery('main')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
