# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-03 15:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20170502_1614'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='knowledgecontrol',
            name='group',
        ),
        migrations.RemoveField(
            model_name='lesson',
            name='group',
        ),
        migrations.RemoveField(
            model_name='timetablegeneralevent',
            name='groups',
        ),
        migrations.RemoveField(
            model_name='timetablegeneralevent',
            name='persons',
        ),
        migrations.RemoveField(
            model_name='vacation',
            name='groups',
        ),
        migrations.RemoveField(
            model_name='vacation',
            name='persons',
        ),
        migrations.AddField(
            model_name='timetableevent',
            name='groups',
            field=models.ManyToManyField(blank=True, to='core.PersonsGroup', verbose_name='Groups'),
        ),
        migrations.AddField(
            model_name='timetableevent',
            name='persons',
            field=models.ManyToManyField(blank=True, to='core.Person', verbose_name='Persons'),
        ),
        migrations.AddField(
            model_name='timetablesubevent',
            name='groups',
            field=models.ManyToManyField(blank=True, to='core.PersonsGroup', verbose_name='Groups'),
        ),
        migrations.AddField(
            model_name='timetablesubevent',
            name='persons',
            field=models.ManyToManyField(blank=True, to='core.Person', verbose_name='Persons'),
        ),
    ]
