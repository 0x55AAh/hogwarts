# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-10 22:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_auto_20170510_2243'),
    ]

    operations = [
        migrations.AddField(
            model_name='timetableevent',
            name='presence_required',
            field=models.BooleanField(default=False, verbose_name='Presence is required'),
        ),
        migrations.AlterField(
            model_name='timetableevent',
            name='organizers',
            field=models.ManyToManyField(blank=True, related_name='core_timetableevent_organizers_events', to='core.Person', verbose_name='Organizers'),
        ),
        migrations.AlterField(
            model_name='timetableevent',
            name='persons',
            field=models.ManyToManyField(blank=True, related_name='core_timetableevent_persons_events', to='core.Person', verbose_name='Persons'),
        ),
    ]
