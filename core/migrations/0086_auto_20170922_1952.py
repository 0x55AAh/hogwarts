# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-09-22 16:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0085_auto_20170915_2054'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='personrole',
            options={'permissions': (('add_group', 'Can add group'),), 'verbose_name': 'Person Role', 'verbose_name_plural': 'Person Roles'},
        ),
    ]
