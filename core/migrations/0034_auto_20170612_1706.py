# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-12 14:06
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0033_auto_20170612_1706'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingsessionpersonactivity',
            name='grade',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Grade', verbose_name='Grade'),
        ),
        migrations.AddField(
            model_name='trainingsessionpersonactivity',
            name='home_work',
            field=models.TextField(blank=True, null=True, verbose_name='Home Work'),
        ),
    ]
