# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-20 14:06
from __future__ import unicode_literals

from django.db import migrations
import django_google_maps.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0054_auto_20170620_1657'),
    ]

    operations = [
        migrations.AddField(
            model_name='educationalinstitution',
            name='geo',
            field=django_google_maps.fields.GeoLocationField(blank=True, max_length=100, null=True),
        ),
    ]
