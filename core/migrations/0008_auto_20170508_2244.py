# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-08 19:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20170508_2140'),
    ]

    operations = [
        migrations.CreateModel(
            name='PersonToInstitutionInvitation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('opened', models.BooleanField(default=True)),
                ('institution_requested', models.BooleanField(default=False, verbose_name='Institution Requested')),
                ('accepted', models.BooleanField(default=False)),
                ('read', models.BooleanField(default=False)),
                ('institution', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='core_persontoinstitutioninvitation_institution_relations', to='core.EducationalInstitution', verbose_name='Institution')),
                ('person', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='core_persontoinstitutioninvitation_person_relations', to='core.Person', verbose_name='Person')),
            ],
            options={
                'verbose_name': 'Person To Institution Invitation',
                'verbose_name_plural': 'Person To Institution Invitations',
            },
        ),
        migrations.CreateModel(
            name='PersonToInstitutionPartnership',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('opened', models.BooleanField(default=True)),
                ('institution_requested', models.BooleanField(default=False, verbose_name='Institution Requested')),
                ('institution', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='core_persontoinstitutionpartnership_institution_relations', to='core.EducationalInstitution', verbose_name='Institution')),
                ('person', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='core_persontoinstitutionpartnership_person_relations', to='core.Person', verbose_name='Person')),
            ],
            options={
                'verbose_name': 'Person To Institution Partnership',
                'verbose_name_plural': 'Person To Institution Partnerships',
            },
        ),
        migrations.CreateModel(
            name='PersonToPersonInvitation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('opened', models.BooleanField(default=True)),
                ('accepted', models.BooleanField(default=False)),
                ('read', models.BooleanField(default=False)),
                ('destination', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='core_persontopersoninvitation_dst_relations', to='core.Person', verbose_name='Destination')),
                ('source', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='core_persontopersoninvitation_src_relations', to='core.Person', verbose_name='Source')),
            ],
            options={
                'verbose_name': 'Person Invitation',
                'verbose_name_plural': 'Person Invitations',
            },
        ),
        migrations.CreateModel(
            name='PersonToPersonPartnership',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('opened', models.BooleanField(default=True)),
                ('destination', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='core_persontopersonpartnership_dst_relations', to='core.Person', verbose_name='Destination')),
                ('source', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='core_persontopersonpartnership_src_relations', to='core.Person', verbose_name='Source')),
            ],
            options={
                'verbose_name': 'Person Partnership',
                'verbose_name_plural': 'Person Partnerships',
            },
        ),
        migrations.AlterModelOptions(
            name='institutiontoinstitutioninvitation',
            options={'verbose_name': 'Institution Invitation', 'verbose_name_plural': 'Institution Invitations'},
        ),
        migrations.AlterModelOptions(
            name='institutiontoinstitutionpartnership',
            options={'verbose_name': 'Institution Partnership', 'verbose_name_plural': 'Institution Partnerships'},
        ),
        migrations.AddField(
            model_name='institutiontoinstitutioninvitation',
            name='read',
            field=models.BooleanField(default=False),
        ),
    ]
