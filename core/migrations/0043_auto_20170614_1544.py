# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-14 12:44
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0042_auto_20170613_2244'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='EducationLevelUpgrade',
            new_name='EducationPeriodEnd',
        ),
        migrations.RenameModel(
            old_name='EducationLevelUpgradePersonActivity',
            new_name='EducationPeriodEndPersonActivity',
        ),
    ]
