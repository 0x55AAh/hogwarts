# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-08-16 17:24
from __future__ import unicode_literals

import core.models.persons
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0066_auto_20170816_2014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='photo',
            field=models.ImageField(upload_to=core.models.persons.person_photos, verbose_name='Photo'),
        ),
    ]
