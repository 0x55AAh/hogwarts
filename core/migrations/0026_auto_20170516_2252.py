# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-16 19:52
from __future__ import unicode_literals

import colorfield.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_auto_20170511_0134'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimetableEventSlot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateTimeField(verbose_name='Start at')),
                ('end', models.DateTimeField(verbose_name='End at')),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('institution', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='event_slots', to='core.EducationalInstitution', verbose_name='Institution')),
            ],
            options={
                'ordering': ['start'],
                'verbose_name': 'Timetable Event Slot',
                'verbose_name_plural': 'Timetable Event Slots',
            },
        ),
        migrations.CreateModel(
            name='TimetableEventsSchema',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('institution', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='schemas', to='core.EducationalInstitution', verbose_name='Institution')),
            ],
            options={
                'verbose_name': 'Timetable Event Schema',
                'verbose_name_plural': 'Timetable Event Schemas',
            },
        ),
        migrations.AddField(
            model_name='personactivity',
            name='home_work',
            field=models.TextField(blank=True, null=True, verbose_name='Home Work'),
        ),
        migrations.AddField(
            model_name='subject',
            name='color',
            field=colorfield.fields.ColorField(default='#47bac1', max_length=10),
        ),
        migrations.AlterField(
            model_name='subject',
            name='institution',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='subjects', to='core.EducationalInstitution', verbose_name='Institution'),
        ),
        migrations.AlterField(
            model_name='timetableevent',
            name='institution',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='events', to='core.EducationalInstitution', verbose_name='Institution'),
        ),
        migrations.AddField(
            model_name='timetableeventslot',
            name='schema',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='event_slots', to='core.TimetableEventsSchema', verbose_name='Schema'),
        ),
        migrations.AddField(
            model_name='timetableevent',
            name='schema_slot',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='core.TimetableEventSlot', verbose_name='Schema Slot'),
        ),
    ]
