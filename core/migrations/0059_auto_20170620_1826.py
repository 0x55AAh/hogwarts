# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-20 15:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0058_auto_20170620_1735'),
    ]

    operations = [
        migrations.RenameField(
            model_name='educationalinstitution',
            old_name='geolocation',
            new_name='geo',
        ),
    ]
