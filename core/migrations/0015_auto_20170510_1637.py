# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-10 13:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20170510_1630'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gradingsystem',
            name='min_pass',
        ),
        migrations.AddField(
            model_name='gradingsystem',
            name='min_pass_grade',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Grade', verbose_name='Minimal pass grade'),
        ),
    ]
