from __future__ import unicode_literals, division
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from polymorphic.models import PolymorphicModel
from django.db import models


@python_2_unicode_compatible
class GroupMembership(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return '[{person}, {group}]'.format(person=self.person, group=self.group)

    class Meta:
        abstract = True


class PupilsGroupMembership(GroupMembership):
    person = models.ForeignKey('core.Pupil', verbose_name=_('Pupil'), on_delete=models.CASCADE)
    group = models.ForeignKey('PupilsGroup', verbose_name=_('Class'), on_delete=models.CASCADE)


class StudentsGroupMembership(GroupMembership):
    STUDIES = (
        ('extramural', _('Extramural')),
    )
    person = models.ForeignKey('core.Student', verbose_name=_('Student'), on_delete=models.CASCADE)
    group = models.ForeignKey('StudentsGroup', verbose_name=_('Group'), on_delete=models.CASCADE)
    study = models.CharField(max_length=128, choices=STUDIES, verbose_name=_('Mode of study'), null=True)
    contract = models.BooleanField(verbose_name=_('Contract'), default=True)


@python_2_unicode_compatible
class PersonsGroup(PolymorphicModel):
    COMMERCIALS = (
        ('speciality', _('Speciality')),
    )
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE, null=True)
    chief = models.ForeignKey('core.Teacher', verbose_name=_('Chief'), on_delete=models.CASCADE, null=True)
    captains = models.ManyToManyField('core.Person', verbose_name=_('Captains'), blank=True,
                                      related_name='%(app_label)s_%(class)s_persons_groups')
    code_name = models.CharField(max_length=10, verbose_name=_('Code name'))
    name_format = models.CharField(max_length=32, verbose_name=_('Name format'), default='{code_name}-{level}')
    commercial_by = models.CharField(max_length=255, verbose_name=_('Commercial by'), choices=COMMERCIALS,
                                     null=True, blank=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        try:
            return self.name_format.format(code_name=self.code_name, level=self.level)
        except KeyError:
            return '{code_name}-{level}'.format(code_name=self.code_name, level=self.level)

    def get_commercial(self):
        return getattr(self, self.commercial_by)

    def is_commercial(self):
        if self.commercial_by:
            return True
        return False

    def get_members(self):
        raise NotImplementedError

    def get_membership(self, person):
        raise NotImplementedError


class PupilsGroup(PersonsGroup):
    level = models.ForeignKey('core.PupilsClass', verbose_name=_('Level'), related_name='groups',
                              on_delete=models.CASCADE)
    members = models.ManyToManyField('core.Pupil', through='PupilsGroupMembership')

    def get_members(self):
        return self.members.filter(pupilsgroupmembership__active=True)

    def get_membership(self, person):
        return self.pupilsgroupmembership_set.get(person=person, active=True)

    class Meta:
        verbose_name = _('Pupils Group')
        verbose_name_plural = _('Pupils Groups')


class StudentsGroup(PersonsGroup):
    level = models.ForeignKey('core.StudentsCourse', verbose_name=_('Level'), related_name='groups',
                              on_delete=models.CASCADE)
    members = models.ManyToManyField('core.Student', through='StudentsGroupMembership')
    speciality = models.ForeignKey('core.Speciality', verbose_name=_('Speciality'), on_delete=models.CASCADE,
                                   related_name='students_groups', null=True)

    def get_members(self):
        return self.members.filter(studentsgroupmembership__active=True)

    def get_membership(self, person):
        return self.studentsgroupmembership_set.get(person=person, active=True)

    class Meta:
        verbose_name = _('Students Group')
        verbose_name_plural = _('Students Groups')
