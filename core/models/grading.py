from __future__ import unicode_literals, division
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.db import models


@python_2_unicode_compatible
class Grade(models.Model):
    grading_system = models.ForeignKey('GradingSystem', verbose_name=_('Grading System'),
                                       on_delete=models.CASCADE, related_name='grades', null=True)
    value = models.CharField(max_length=255, verbose_name=_('Value'), unique=True)
    code = models.CharField(max_length=255, verbose_name=_('Code'), null=True, blank=True)
    description = models.TextField(verbose_name=_('Description'), blank=True)
    notes = models.TextField(verbose_name=_('Notes'), blank=True)
    check_pass = models.BooleanField(default=True, verbose_name=_('Check Pass'))
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return self.value

    @classmethod
    def make_grades(cls, values_range, **kwargs):
        value_from, value_to = values_range
        return [
            cls(value=str(value), **kwargs) for value
            in range(value_from, value_to + 1)
        ]

    class Meta:
        verbose_name = _('Grade')
        verbose_name_plural = _('Grades')


@python_2_unicode_compatible
class GradingSystem(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Name'), null=True, unique=True)
    description = models.TextField(verbose_name=_('Description'), blank=True)
    cumulative = models.BooleanField(verbose_name=_('Cumulative'), default=False)
    differential = models.BooleanField(verbose_name=_('Differential'), default=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def get_grades(self):
        return self.grades.filter(active=True)

    def get_grade(self, value):
        return self.get_grades().get(value=value)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Grading System')
        verbose_name_plural = _('Grading Systems')


def ects():
    grading_system = GradingSystem.objects.create(name=_('ECTS'), cumulative=True)
    grades = Grade.make_grades(values_range=[90, 100], grading_system=grading_system, check_pass=True, code='A',
                               description=_(''),
                               notes=_('Outstanding performance with only minor errors'))
    grades += Grade.make_grades(values_range=[82, 89], grading_system=grading_system, check_pass=True, code='B',
                                description=_(''),
                                notes=_('Above the average standard but with some errors'))
    grades += Grade.make_grades(values_range=[75, 81], grading_system=grading_system, check_pass=True, code='C',
                                description=_(''),
                                notes=_('Generally sound work with a number of notable errors'))
    grades += Grade.make_grades(values_range=[67, 74], grading_system=grading_system, check_pass=True, code='D',
                                description=_(''),
                                notes=_('Fair but with significant shortcomings'))
    grades += Grade.make_grades(values_range=[60, 66], grading_system=grading_system, check_pass=True, code='E',
                                description=_(''),
                                notes=_('Performance meets the minimum criteria'))
    grades += Grade.make_grades(values_range=[35, 59], grading_system=grading_system, check_pass=False, code='FX',
                                description=_(''),
                                notes=_('Fail - some more work required before the credit can be awarded'))
    grades += Grade.make_grades(values_range=[1, 34], grading_system=grading_system, check_pass=False, code='F',
                                description=_(''),
                                notes=_('Fail - considerable further work is required'))
    Grade.objects.bulk_create(grades)


def p5():
    grading_system = GradingSystem.objects.create(name=_('5 Point'))
    Grade.objects.bulk_create([
        Grade(grading_system=grading_system, value='1', check_pass=False, description=_('Very Poor'),
              notes=_('The lowest possible grade, denotes complete failure, and is very rarely used')),
        Grade(grading_system=grading_system, value='2', check_pass=False, description=_('Unsatisfactory'),
              notes=_('Denotes hardly any knowledge, below average, the first level of failing')),
        Grade(grading_system=grading_system, value='3', check_pass=True, description=_('Satisfactory'),
              notes=_('Denotes a creditable or passing grade')),
        Grade(grading_system=grading_system, value='4', check_pass=True, description=_('Good'),
              notes=_('Denotes good knowledge of a subject')),
        Grade(grading_system=grading_system, value='5', check_pass=True, description=_('Excellent'),
              notes=_('Denotes highest distinction and excellent knowledge of a subject')),
    ])


def p12():
    grading_system = GradingSystem.objects.create(name=_('12 Point'))
    Grade.objects.bulk_create([
        Grade(grading_system=grading_system, value='1', check_pass=False,
              description=_('Very Poor'), notes=_('')),
        Grade(grading_system=grading_system, value='2', check_pass=False,
              description=_('Poor'), notes=_('')),
        Grade(grading_system=grading_system, value='3', check_pass=False,
              description=_('Almost Adequate'), notes=_('')),
        Grade(grading_system=grading_system, value='4', check_pass=True,
              description=_('Adequate'), notes=_('')),
        Grade(grading_system=grading_system, value='5', check_pass=True,
              description=_('Satisfactory'), notes=_('')),
        Grade(grading_system=grading_system, value='6', check_pass=True,
              description=_('Above satisfactory'), notes=_('')),
        Grade(grading_system=grading_system, value='7', check_pass=True,
              description=_('Fairly good'), notes=_('')),
        Grade(grading_system=grading_system, value='8', check_pass=True,
              description=_('Good'), notes=_('')),
        Grade(grading_system=grading_system, value='9', check_pass=True,
              description=_('Very good'), notes=_('')),
        Grade(grading_system=grading_system, value='10', check_pass=True,
              description=_('Almost excellent'), notes=_('')),
        Grade(grading_system=grading_system, value='11', check_pass=True,
              description=_('Excellent'), notes=_('')),
        Grade(grading_system=grading_system, value='12', check_pass=True,
              description=_('Perfect'), notes=_('')),
    ])


def pass_fail():
    grading_system = GradingSystem.objects.create(name='Pass or Fail', differential=False)
    Grade.objects.bulk_create([
        Grade(grading_system=grading_system, value='Pass', check_pass=True, description=_('Pass'), notes=_('Pass')),
        Grade(grading_system=grading_system, value='Fail', check_pass=False, description=_('Fail'), notes=_('Fail'))
    ])
