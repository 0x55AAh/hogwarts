from __future__ import unicode_literals, division
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


@python_2_unicode_compatible
class Page(models.Model):
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.slug

    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')


@python_2_unicode_compatible
class Email(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE, null=True)
    email = models.EmailField(verbose_name=_('Email'))
    description = models.TextField(blank=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _('Email')
        verbose_name_plural = _('Emails')


@python_2_unicode_compatible
class Phone(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE, null=True)
    number = PhoneNumberField(verbose_name=_('Number'))
    description = models.TextField(blank=True)

    def __str__(self):
        return str(self.number)

    class Meta:
        verbose_name = _('Phone')
        verbose_name_plural = _('Phones')
