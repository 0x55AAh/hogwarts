from __future__ import unicode_literals, division
from django.utils.encoding import python_2_unicode_compatible, force_text
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from polymorphic.models import PolymorphicModel
from django.db import models
from utils.functools import CheckerMixin


@python_2_unicode_compatible
class EducationLevel(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE, related_name='%(app_label)s_%(class)ss', null=True)
    subjects = models.ManyToManyField('core.Subject', verbose_name=_('Subjects'))
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return str(self.number)

    def get_groups(self):
        return self.groups.filter(active=True)

    class Meta:
        abstract = True


class PupilsClass(EducationLevel):
    NUMBERS = [(n, str(n)) for n in range(1, 13)]
    number = models.IntegerField(default=1, choices=NUMBERS, verbose_name=_('Number'))

    class Meta:
        verbose_name = _('Pupils Class')
        verbose_name_plural = _('Pupils Classes')


class StudentsCourse(EducationLevel):
    NUMBERS = [(n, str(n)) for n in range(1, 6)]
    number = models.IntegerField(default=1, choices=NUMBERS, verbose_name=_('Number'))
    speciality = models.ForeignKey('core.Speciality', verbose_name=_('Speciality'), on_delete=models.CASCADE,
                                   related_name='courses', null=True)

    class Meta:
        verbose_name = _('Students Course')
        verbose_name_plural = _('Students Courses')


@python_2_unicode_compatible
class PersonBaseActivity(PolymorphicModel):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'), null=True,
                                    related_name='%(app_label)s_%(class)s_activities', on_delete=models.CASCADE)
    event = models.ForeignKey('core.TimetableEvent', related_name='%(app_label)s_%(class)s_activities',
                              verbose_name=_('Event'), on_delete=models.CASCADE)
    person = models.ForeignKey('core.Person', related_name='%(app_label)s_%(class)s_activities',
                               verbose_name=_('Person'), on_delete=models.CASCADE, null=True)
    group = models.ForeignKey('core.PersonsGroup', verbose_name=_('Group'), on_delete=models.CASCADE,
                              null=True, blank=True)
    note = models.TextField(verbose_name=_('Note'), blank=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return force_text(self.person)

    class Meta:
        abstract = True


class PersonEventRequirementsActivity(PersonBaseActivity):
    done = models.BooleanField(default=False, verbose_name=_('Done'))
    fond = models.IntegerField(verbose_name=_('Fond'), default=0)

    class Meta:
        verbose_name = _('Requirements Activity')
        verbose_name_plural = _('Requirements Activities')


class PersonActivity(PersonBaseActivity):
    present = models.BooleanField(verbose_name=_('Present'), default=False)
    start = models.DateTimeField(verbose_name=_('Start at'), null=True, blank=True)
    end = models.DateTimeField(verbose_name=_('End at'), null=True, blank=True)

    def set_late_time(self, time):
        self.start = time() if callable(time) else time

    def set_late_now(self):
        self.set_late_time(now)

    def set_leave_time(self, time):
        self.end = time() if callable(time) else time

    def set_leave_now(self):
        self.set_leave_time(now)

    class Meta:
        ordering = ['start']
        verbose_name = _('Activity')
        verbose_name_plural = _('Activities')


class TrainingSessionPersonActivity(PersonActivity):
    grade = models.ForeignKey('core.Grade', verbose_name=_('Grade'), on_delete=models.CASCADE, null=True, blank=True)
    home_work = models.TextField(verbose_name=_('Home Work'), null=True, blank=True)

    class Meta(PersonActivity.Meta):
        verbose_name = _('Training Session Activity')
        verbose_name_plural = _('Training Session Activities')


class PaymentPersonActivity(PersonActivity):
    value = models.IntegerField(verbose_name=_('Value'), default=0)
    underpaid = models.IntegerField(verbose_name=_('Underpaid'), default=0)
    overpaid = models.IntegerField(verbose_name=_('Overpaid'), default=0)

    def __str__(self):
        return str(self.value)

    def rebuild(self):
        diff = self.event.value - self.value
        if diff > 0:
            self.underpaid = diff
        elif diff < 0:
            self.overpaid = diff
        else:
            self.underpaid = self.overpaid = 0

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.rebuild()
        super(PaymentPersonActivity, self).save(force_insert, force_update, using, update_fields)

    class Meta(PersonActivity.Meta):
        verbose_name = _('Payment Activity')
        verbose_name_plural = _('Payment Activities')


class EducationPeriodPersonActivity(PersonActivity):
    scholarship = models.ForeignKey('core.Scholarship', verbose_name=_('Scholarship'), on_delete=models.CASCADE,
                                    null=True, blank=True)

    def update_scholarship(self):
        if self.previous_period_end:
            self.scholarship = self.previous_period_end.make_scholarship()
            self.save()

    @property
    def previous_period_end(self):
        if self.event.previous:
            return self.event.previous.period_end

    class Meta(PersonActivity.Meta):
        verbose_name = _('Education Period Activity')
        verbose_name_plural = _('Education Period Activities')


class EducationPeriodEndPersonActivity(PersonActivity, CheckerMixin):
    check_pass = models.BooleanField(default=False, verbose_name=_('Pass'))

    def payments_check(self):
        if not self.group.is_commercial():
            return True
        if not getattr(self.group.get_membership(self.person), 'contract', False):
            return True
        underpaid_payments = PaymentPersonActivity.objects.filter(
            event__in=self.event.payments.filter(active=True),
            person=self.person,
            group=self.group,
            underpaid__gt=0
        )
        if underpaid_payments:
            return False
        return True

    def get_knowledge_controls_grades(self,  differential=None):
        knowledge_controls = TrainingSessionPersonActivity.objects.filter(
            event__in=self.event.knowledge_controls.filter(active=True),
            person=self.person,
            group=self.group
        )
        if differential is None:
            return [control.grade for control in knowledge_controls]
        return [
            control.grade for control in knowledge_controls
            if control.grade.grading_system == differential
        ]

    def knowledge_controls_check(self, differential=None):
        grades = self.get_knowledge_controls_grades(differential)
        if all(grade.check_pass for grade in grades):
            return True
        return False

    def update_checkers_results(self):
        self.check_pass = all(self.checkers_results())
        self.save()

    def make_scholarship(self):
        if self.knowledge_controls_check(differential=False):
            differential_grades = self.get_knowledge_controls_grades(differential=True)

    class Meta(PersonActivity.Meta):
        verbose_name = _('Education Level Upgrade Activity')
        verbose_name_plural = _('Education Level Upgrade Activities')
