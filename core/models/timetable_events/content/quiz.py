from __future__ import unicode_literals, division

from django.db import models
from django.utils.translation import ugettext_lazy as _

from .base import TimetableEventContent


class Quiz(TimetableEventContent):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='quizzes', on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    description = models.TextField(verbose_name=_('Description'), null=True, blank=True)
    active = models.BooleanField(verbose_name=_('Active'), default=True)

    def get_questions(self):
        return self.questions.filter(active=True).order_by('?')

    class Meta:
        verbose_name = _('Quiz')
        verbose_name_plural = _('Quizzes')


class QuizAnswer(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='answers', on_delete=models.CASCADE)
    question = models.ForeignKey('QuizQuestion', verbose_name=_("Question"), related_name='answers',
                                 on_delete=models.CASCADE)
    right = models.BooleanField(verbose_name=_('Is right?'), default=False)
    active = models.BooleanField(verbose_name=_('Active'), default=True)

    class Meta:
        verbose_name = _('Quiz answer')
        verbose_name_plural = _('Quiz answers')


class QuizQuestion(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='questions', on_delete=models.CASCADE)
    quiz = models.ForeignKey('Quiz', verbose_name=_('Quiz'), related_name='questions', on_delete=models.CASCADE)
    value = models.TextField(verbose_name=_('Value'))
    active = models.BooleanField(verbose_name=_('Active'), default=True)

    def get_all_answers(self):
        return self.answers.filter(active=True).order_by('?')

    def get_right_answers(self):
        return self.answers.filter(right=True, active=True)

    def check_answers(self, answers):
        right_answers = set(self.get_right_answers())
        answers = set(answers)
        return answers == right_answers

    class Meta:
        verbose_name = _('Quiz question')
        verbose_name_plural = _('Quiz questions')
