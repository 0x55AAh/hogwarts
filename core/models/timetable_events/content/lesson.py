from __future__ import unicode_literals, division

from django.db import models
from django.utils.translation import ugettext_lazy as _

from .base import TimetableEventContent


class Lesson(TimetableEventContent):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='quizzes', on_delete=models.CASCADE)
    topic = models.CharField(max_length=255, verbose_name=_('Topic'))
    description = models.TextField(verbose_name=_('Description'), null=True, blank=True)
    active = models.BooleanField(verbose_name=_('Active'), default=True)

    def get_blocks(self):
        return self.lesson_blocks.filter(active=True)

    class Meta:
        verbose_name = _('Lesson')
        verbose_name_plural = _('Lessons')


class LessonBlock(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='lesson_blocks', on_delete=models.CASCADE)
    lesson = models.ForeignKey('Lesson', verbose_name=_('Lesson'), related_name='lesson_blocks',
                               on_delete=models.CASCADE)
    title = models.CharField(max_length=255, verbose_name=_('Title'))
    content = models.TextField(verbose_name='Content', blank=True)

    class Meta:
        verbose_name = _('Lesson Block')
        verbose_name_plural = _('Lesson Blocks')
