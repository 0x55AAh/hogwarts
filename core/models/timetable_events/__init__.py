from __future__ import unicode_literals, division
from django.utils.encoding import python_2_unicode_compatible, force_text
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.db import models
from polymorphic.models import PolymorphicModel
from colorfield.fields import ColorField

from ..timetable_events.content.base import TimetableEventContent
from ..education_process import (
    PersonActivity, TrainingSessionPersonActivity, PaymentPersonActivity,
    EducationPeriodEndPersonActivity, EducationPeriodPersonActivity,
    PersonEventRequirementsActivity
)
from django.db.models import Q
from dateutils import relativedelta


class MembersMixin(object):
    groups = None
    persons = None

    def get_group_members(self):
        from functools import reduce
        members_set = set(reduce(
            lambda a, x: a + x,
            [list(group.get_members()) for group in self.groups.filter(active=True)]
        ))
        return list(members_set)

    def get_group_members_with_groups(self):
        members_with_groups = set()
        for group in self.groups.filter(active=True):
            members = group.get_members()
            members_with_groups.update(zip([group] * members.count(), members))
        return list(members_with_groups)

    def get_personal_members(self):
        return list(self.persons.filter(active=True))

    def get_personal_members_with_none_group(self):
        persons = self.get_personal_members()
        return list(zip([None] * len(persons), persons))

    def get_members(self):
        members_set = set(self.get_group_members() + self.get_personal_members())
        return list(members_set)

    def get_members_with_group(self):
        members_set = set(self.get_group_members_with_groups() + self.get_personal_members_with_none_group())
        return list(members_set)


@python_2_unicode_compatible
class TimetableEventsScheme(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='schemes', on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    slots_limit = models.IntegerField(verbose_name=_('Slots Limit'), default=0)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return self.name

    def get_slots(self, limit=0):
        limit = limit or self.slots_limit
        slots = self.event_slots.filter(active=True)
        if limit:
            return slots[:limit]
        return slots

    def get_slots_durations(self, limit=0):
        limit = limit or self.slots_limit
        durations = [
            relativedelta(slot.end, slot.start)
            for slot in self.get_slots(limit)
        ]
        return [
            duration.days * 24 * 60 +
            duration.hours * 60 +
            duration.minutes
            for duration in durations
        ]

    def get_slot_by_order(self, order):
        for slot in self.get_slots():
            if slot.order == order:
                return slot

    class Meta:
        verbose_name = _('Timetable Event Scheme')
        verbose_name_plural = _('Timetable Event Schemes')


class TimetableEventSlot(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='event_slots', on_delete=models.CASCADE, null=True)
    scheme = models.ForeignKey('TimetableEventsScheme', verbose_name=_('Scheme'),
                               related_name='event_slots', on_delete=models.CASCADE, null=True)
    start = models.DateTimeField(verbose_name=_('Start at'))
    end = models.DateTimeField(verbose_name=_('End at'))
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    @property
    def order(self):
        slots = self.__class__.objects.filter(active=True)
        for i, slot in enumerate(slots):
            if slot == self:
                return i + 1

    class Meta:
        ordering = ['start']
        verbose_name = _('Timetable Event Slot')
        verbose_name_plural = _('Timetable Event Slots')


@python_2_unicode_compatible
class TimetableEventRequirement(MembersMixin, models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='requirements', on_delete=models.CASCADE, null=True)
    description = models.TextField(verbose_name=_('Description'), null=True, blank=True)
    fond = models.IntegerField(verbose_name=_('Fond'), default=0)
    event = models.OneToOneField('TimetableEvent', verbose_name=_('Event'), on_delete=models.CASCADE, null=True)
    start = models.DateTimeField(verbose_name=_('Start at'), default=now)
    done = models.BooleanField(default=False, verbose_name=_('Done'))
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __init__(self, *args, **kwargs):
        super(TimetableEventRequirement, self).__init__(*args, **kwargs)
        self.persons = self.event.persons
        self.groups = self.event.persons

    def __str__(self):
        return 'Requirements for {event}'.format(event=self.event)

    @property
    def duration(self):
        return relativedelta(self.end, self.start)

    @property
    def end(self):
        return self.event.start

    def create_activities(self):
        activities = [
            PersonEventRequirementsActivity(
                institution=self.institution,
                group=group,
                person=person,
                event=self.event
            )
            for group, person in self.get_members_with_group()
        ]
        return PersonEventRequirementsActivity.objects.bulk_create(activities)

    def begin(self):
        self.create_activities()

    def finish(self):
        self.active = False
        self.get_activities().update(active=False)
        self.save(update_fields=['active'])

    def check_done(self):
        """
        Run after all activities done
        """
        self.done = True
        self.save(update_fields=['done'])

    def get_activities(self):
        return self.core_personeventrequirementsactivity_activities.filter(active=True)

    class Meta:
        ordering = ['start']
        verbose_name = _('Timetable Event Requirement')
        verbose_name_plural = _('Timetable Event Requirements')


@python_2_unicode_compatible
class EventAbsenceReason(models.Model):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='absence_reasons', on_delete=models.CASCADE, null=True)
    event = models.ForeignKey('core.TimetableEvent', verbose_name=_('Event'), on_delete=models.CASCADE)
    group = models.ForeignKey('core.PersonsGroup', verbose_name=_('Group'), on_delete=models.CASCADE, blank=True,
                              related_name='absence_reasons')
    person = models.ForeignKey('core.Person', verbose_name=_('Person'), on_delete=models.CASCADE, blank=True,
                               related_name='absence_reasons')
    description = models.TextField(verbose_name=_('Description'))
    attachment = models.FileField(verbose_name=_('Attachment'), null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.person_or_group_check()
        super(EventAbsenceReason, self).save(force_insert, force_update, using, update_fields)

    def person_or_group_check(self):
        if all([self.person, self.group]) or not any([self.person, self.group]):
            raise ValueError('Define any person or group to create absence reason')

    def get_person_or_group(self):
        self.person_or_group_check()
        return self.person or self.group

    def __str__(self):
        return '{event},{person_or_group} => {description}'.format(
            event=self.event,
            person_or_group=self.get_person_or_group(),
            description=self.description
        )

    class Meta:
        verbose_name = _('Event Absence Reason')
        verbose_name_plural = _('Event Absence Reasons')


@python_2_unicode_compatible
class TimetableEvent(MembersMixin, PolymorphicModel):
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='events', on_delete=models.CASCADE, null=True)

    organizers = models.ManyToManyField('core.Person', verbose_name=_('Organizers'), blank=True,
                                        related_name='%(app_label)s_%(class)s_organizers_events')
    groups = models.ManyToManyField('core.PersonsGroup', verbose_name=_('Groups'), blank=True)
    persons = models.ManyToManyField('core.Person', verbose_name=_('Persons'), blank=True,
                                     related_name='%(app_label)s_%(class)s_persons_events')
    absent_groups = models.ManyToManyField('core.PersonsGroup', verbose_name=_('Absent groups'), blank=True,
                                           related_name='%(app_label)s_%(class)s_absent_groups_events',
                                           through='EventAbsenceReason')
    absent_persons = models.ManyToManyField('core.Person', verbose_name=_('Absent persons'), blank=True,
                                            related_name='%(app_label)s_%(class)s_absent_persons_events',
                                            through='EventAbsenceReason')

    sub_events = models.ManyToManyField('self', verbose_name=_('Sub Events'), blank=True)
    note = models.TextField(verbose_name=_('Note'), null=True, blank=True)
    room = models.ForeignKey('core.LectureRoom', verbose_name=_('Room'), on_delete=models.CASCADE,
                             null=True, blank=True)
    scheme_slot = models.ForeignKey('TimetableEventSlot', verbose_name=_('Scheme Slot'),
                                    on_delete=models.CASCADE, null=True, blank=True)
    start = models.DateTimeField(verbose_name=_('Start at'))
    end = models.DateTimeField(verbose_name=_('End at'))
    color = ColorField(default='#47bac1')
    sub_event = models.BooleanField(default=False, verbose_name=_('Sub Event'))
    presence_required = models.BooleanField(default=False, verbose_name=_('Presence is required'))
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    person_activity_model = None

    def __str__(self):
        return force_text(self.get_real_instance().name)

    @property
    def requirement(self):
        return self.timetableeventrequirement

    def event_slot_changed(self):
        self.start = self.scheme_slot.start
        self.end = self.scheme_slot.end

    @staticmethod
    def update_events_scheme(events, scheme):
        for event in events:
            event.scheme_slot = scheme.get_slot_by_order(order=event.order)
            event.event_slot_changed()
            events.save(update_fields=['scheme_slot'])

    def is_period_available(self):
        if not self.room:
            return True
        if self.institution.events.filter(room=self.room, active=True) \
                .exclude(pk=self.pk).filter(
                            Q(start__gte=self.start) & Q(start__lte=self.end) |
                            Q(end__gte=self.start) & Q(end__lte=self.end) |
                            Q(start__lte=self.start) & Q(end__gte=self.end) |
                            Q(start__gte=self.start) & Q(end__lte=self.end)):
            return False
        return True

    def save(self, *args, **kwargs):
        if not self.is_period_available():
            raise ValueError('Period is not available')
        super(TimetableEvent, self).save(*args, **kwargs)

    def begin(self):
        self.create_activities()

    def finish(self):
        self.active = False
        if self.room:
            self.room.free = True
            self.get_activities().update(active=False)
        self.save()

    @property
    def duration(self):
        return relativedelta(self.end, self.start)

    def _create_activities(self, event=None):
        event = event or self
        return [
            self.person_activity_model(institution=self.institution, group=group, person=person, event=event)
            for group, person in self.get_members_with_group()
        ]

    def create_activities(self):
        from functools import reduce
        if self.sub_events:
            activities = reduce(
                lambda a, x: a + x,
                [self._create_activities(event) for event in self.sub_events.filter(active=True)]
            )
        else:
            activities = self._create_activities()
        return self.person_activity_model.objects.bulk_create(activities)

    def get_activities(self):
        return self.core_personactivity_activities.filter(active=True)

    class Meta:
        ordering = ['start']
        verbose_name = _('Event')
        verbose_name_plural = _('Timetable')


class BaseTrainingSession(TimetableEvent):
    subject = models.ForeignKey('core.Subject', verbose_name=_('Subject'), on_delete=models.CASCADE, null=True)
    content = models.ForeignKey(TimetableEventContent, verbose_name=_('Content'), on_delete=models.CASCADE,
                                null=True, blank=True)
    person_activity_model = TrainingSessionPersonActivity

    def __str__(self):
        return '{}:{}'.format(self.name, self.subject.name)

    class Meta:
        abstract = True


class Vacation(TimetableEvent):
    name = models.CharField(max_length=128, verbose_name=_('Name'), default=_('Vacation'))
    person_activity_model = PersonActivity

    class Meta:
        verbose_name = _('Vacation')
        verbose_name_plural = _('Vacation')


class TimetableGeneralEvent(TimetableEvent):
    name = models.CharField(max_length=128, verbose_name=_('Name'), null=True)
    person_activity_model = PersonActivity

    class Meta:
        verbose_name = _('Timetable General Event')
        verbose_name_plural = _('Timetable General Events')


class KnowledgeControl(BaseTrainingSession):
    TYPES = (
        ('final', _('Final')),
        ('module', _('Module')),
        ('other', _('Other'))
    )
    name = models.CharField(max_length=128, verbose_name=_('Name'), default=_('Knowledge Control'))
    type = models.CharField(max_length=128, verbose_name=_('Type'), choices=TYPES)
    grading_system = models.ForeignKey('core.GradingSystem', verbose_name=_('Grading System'),
                                       on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = _('Knowledge Control')
        verbose_name_plural = _('Knowledge Controls')


class TrainingSession(BaseTrainingSession):
    name = models.CharField(max_length=128, verbose_name=_('Name'), default=_('Training Session'))

    class Meta:
        verbose_name = _('Training Session')
        verbose_name_plural = _('Training Sessions')


class Payment(TimetableEvent):
    name = models.CharField(max_length=128, verbose_name=_('Name'), default=_('Payment'))
    value = models.IntegerField(verbose_name=_('Value'), default=0)
    person_activity_model = PaymentPersonActivity

    def __str__(self):
        return str(self.value)

    class Meta:
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')


class EducationPeriod(TimetableEvent):
    name = models.CharField(max_length=128, verbose_name=_('Name'))
    education_level_changed = models.BooleanField(verbose_name=_('Education level changed'), default=False)
    scholarship_changed = models.BooleanField(verbose_name=_('Scholarship changed'), default=False)
    previous = models.ForeignKey('self', on_delete=models.CASCADE, verbose_name=_('Previous'), null=True, blank=True)
    person_activity_model = EducationPeriodPersonActivity

    class Meta:
        verbose_name = _('Education Period')
        verbose_name_plural = _('Education Periods')


class EducationPeriodEnd(TimetableEvent):
    name = models.CharField(max_length=128, verbose_name=_('Name'))
    payments = models.ManyToManyField('core.Payment', verbose_name=_('Payments'))
    knowledge_controls = models.ManyToManyField('core.KnowledgeControl', verbose_name=_('Knowledge controls'))
    period = models.OneToOneField('EducationPeriod', verbose_name=_('Period'), on_delete=models.CASCADE,
                                  related_name='period_end', null=True)
    person_activity_model = EducationPeriodEndPersonActivity

    class Meta:
        verbose_name = _('Education Level Upgrade')
        verbose_name_plural = _('Education Levels Upgrades')
