from __future__ import unicode_literals, division
from django.contrib.auth.models import AbstractUser
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings
from django.utils.timezone import now
from dateutils import relativedelta
from polymorphic.models import PolymorphicModel
from colorfield.fields import ColorField
from languages.fields import LanguageField
from sorl.thumbnail import ImageField
from messenger.settings import PERSONAL_GROUP_PREFIX
from ui.fields import ThumbnailField
from django.db.models.signals import post_save
from model_utils import Choices
import os


def person_photos(instance, filename):
    return os.path.join(
        'profiles', str(instance.institution.pk),
        str(instance.user.pk), filename
    )


@python_2_unicode_compatible
class PersonNetworkStatus(models.Model):
    STATUS = Choices(
        ('online', _('Online')),
        ('offline', _('Offline')),
    )
    DEVICE = Choices(
        ('mobile', _('Mobile')),
        ('pc', _('PC')),
        ('unknown', _('Unknown')),
    )
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE, null=True)
    person = models.OneToOneField('Person', null=True, verbose_name=_('Person'), related_name='network_status',
                                  on_delete=models.CASCADE)
    value = models.CharField(max_length=128, verbose_name=_('Value'), choices=STATUS, default=STATUS.offline)
    updated = models.DateTimeField(verbose_name=_('Updated'), auto_now=True)
    device = models.CharField(max_length=128, verbose_name=_('Device'), default=DEVICE.unknown)

    def __str__(self):
        return '{}:{}'.format(self.person, self.value)

    def _set_status(self, name, device=None):
        update_fields = ['updated']
        new_value = getattr(self.STATUS, name)
        if self.value != new_value:
            self.value = new_value
            update_fields.append('value')
        if device:
            new_device = getattr(self.STATUS, device)
            if self.device != new_device:
                self.device = new_device
                update_fields.append('device')
        self.save(update_fields=update_fields)

    def online(self, device=None):
        self._set_status('online', device)

    def offline(self):
        self._set_status('offline')

    def is_online(self):
        return self.value == self.STATUS.online

    def is_offline(self):
        return self.value == self.STATUS.offline

    class Meta:
        verbose_name = _('Person Network Status')
        verbose_name_plural = _('Person Network Statuses')


@python_2_unicode_compatible
class PersonRole(models.Model):
    ROLES = Choices(
        ('teacher', _('Teacher')),
        ('pupil', _('Pupil')),
        ('student', _('Student')),
    )
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=128, verbose_name=_('Name'), choices=ROLES)
    name_other = models.CharField(max_length=128, verbose_name=_('Other Name'), null=True, blank=True)
    person = models.ForeignKey('Person', null=True, verbose_name=_('Person'), related_name='roles',
                               on_delete=models.CASCADE)
    start = models.DateTimeField(verbose_name=_('Start at'), default=now)
    end = models.DateTimeField(verbose_name=_('End at'), null=True, blank=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return self.get_name()

    def get_name(self):
        return self.name or self.name_other

    @property
    def duration(self):
        return relativedelta(self.end, self.start)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.person.get_roles().filter(end__isnull=True).update(end=now)
        super(PersonRole, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = _('Person Role')
        verbose_name_plural = _('Person Roles')


class User(AbstractUser):
    default_profile = models.ForeignKey('Person', verbose_name=_('Default profile'), on_delete=models.CASCADE,
                                        related_name='+', null=True, blank=True)

    @property
    def active_profiles(self):
        return self.profiles.filter(active=True)

    def get_current_profile(self):
        if self.default_profile in self.active_profiles:
            return self.default_profile
        else:
            return self.active_profiles.first()

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'


@python_2_unicode_compatible
class Person(PolymorphicModel):
    SEX = Choices(
        ('male', _('Male')),
        ('female', _('Female'))
    )
    institution = models.ForeignKey('core.EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='persons', on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('User'), related_name='profiles',
                             on_delete=models.CASCADE, null=True)
    first_name = models.CharField(max_length=128, verbose_name=_('First name'))
    second_name = models.CharField(max_length=128, verbose_name=_('Second name'))
    last_name = models.CharField(max_length=128, verbose_name=_('Last name'))
    sex = models.CharField(max_length=6, choices=SEX, verbose_name=_('Sex'), null=True)

    photo = ImageField(upload_to=person_photos, verbose_name=_('Photo'))
    photo_thumbnail_square = ThumbnailField('photo', size=(100, 100), blank=True)

    cover = ImageField(upload_to=person_photos, verbose_name=_('Cover'), null=True, blank=True)
    cover_thumbnail = ThumbnailField('cover', size=(1493, 1119), blank=True)

    emails = models.ManyToManyField('core.Email', verbose_name=_('Emails'), blank=True)
    phones = models.ManyToManyField('core.Phone', verbose_name=_('Phones'), blank=True)
    birthday = models.DateField(verbose_name=_('Birthday'))
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    color = ColorField(default='#47bac1')
    language = LanguageField(default='en')
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        return '{last_name} {first_name} {second_name}'.format(
            first_name=self.first_name.strip(),
            second_name=self.second_name.strip(),
            last_name=self.last_name.strip()
        )

    @property
    def age(self):
        return relativedelta(now(), self.birthday)

    def get_roles(self):
        return self.roles.filter(active=True)

    def get_current_role(self):
        return self.get_roles().last()

    def get_activities(self, datetime_range=None, **kwargs):
        if len(datetime_range) != 2 or not all(datetime_range):
            raise ValueError('Datetime range must have 2 datetime values')

        return self.core_personactivity_activities.filter(
            event__start__range=datetime_range,
            event__end__range=datetime_range,
            **kwargs
        )

    def get_underpaid_payments(self):
        return self.core_personactivity_activities\
            .filter(paymentpersonactivity__underpaid__gt=0)

    @property
    def new_messages(self):
        from messenger.models import ClientMessage
        return ClientMessage.new_messages(self)

    @property
    def channels_group(self):
        from channels import Group
        return Group('%s_%s' % (PERSONAL_GROUP_PREFIX, self.pk))

    def disconnect(self):
        self.channels_group.send({'close': True})

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')


class Pupil(Person):
    def get_group(self):
        return self.pupilsgroup_set.get(active=True)

    class Meta:
        verbose_name = _('Pupil')
        verbose_name_plural = _('Pupils')


class Student(Person):
    def get_group(self):
        return self.studentsgroup_set.get(active=True)

    class Meta:
        verbose_name = _('Student')
        verbose_name_plural = _('Students')


class Teacher(Person):
    subjects = models.ManyToManyField('core.Subject', verbose_name=_('Subjects'), blank=True)

    def get_groups(self):
        return self.personsgroup_set.filter(active=True)

    class Meta:
        verbose_name = _('Teacher')
        verbose_name_plural = _('Teachers')


def create_person_network_status(sender, instance, **kwargs):
    if kwargs['created']:
        PersonNetworkStatus.objects.create(institution=instance.institution, person=instance)


post_save.connect(create_person_network_status, sender=Teacher)
post_save.connect(create_person_network_status, sender=Student)
post_save.connect(create_person_network_status, sender=Pupil)
