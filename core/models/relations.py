from __future__ import unicode_literals, division
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.db import models


class Relation(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    opened = models.BooleanField(default=True)

    def close(self):
        self.opened = False
        self.save()
        return self

    class Meta:
        abstract = True


@python_2_unicode_compatible
class InstitutionToInstitutionRelation(Relation):
    source = models.ForeignKey('core.EducationalInstitution',
                               verbose_name=_('Source'),
                               related_name='%(app_label)s_%(class)s_src_relations',
                               on_delete=models.CASCADE,
                               null=True)
    destination = models.ForeignKey('core.EducationalInstitution',
                                    verbose_name=_('Destination'),
                                    related_name='%(app_label)s_%(class)s_dst_relations',
                                    on_delete=models.CASCADE,
                                    null=True)

    def __str__(self):
        return '[%s, %s]' % (self.source, self.destination)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class PersonToPersonRelation(Relation):
    source = models.ForeignKey('core.Person',
                               verbose_name=_('Source'),
                               related_name='%(app_label)s_%(class)s_src_relations',
                               on_delete=models.CASCADE,
                               null=True)
    destination = models.ForeignKey('core.Person',
                                    verbose_name=_('Destination'),
                                    related_name='%(app_label)s_%(class)s_dst_relations',
                                    on_delete=models.CASCADE,
                                    null=True)

    def __str__(self):
        return '[%s, %s]' % (self.source, self.destination)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class PersonToInstitutionRelation(Relation):
    institution = models.ForeignKey('core.EducationalInstitution',
                                    verbose_name=_('Institution'),
                                    related_name='%(app_label)s_%(class)s_institution_relations',
                                    on_delete=models.CASCADE,
                                    null=True)
    person = models.ForeignKey('core.Person',
                               verbose_name=_('Person'),
                               related_name='%(app_label)s_%(class)s_person_relations',
                               on_delete=models.CASCADE,
                               null=True)
    institution_requested = models.BooleanField(verbose_name=_('Institution Requested'), default=False)

    @property
    def source(self):
        return self.institution if self.institution_requested else self.person

    @property
    def destination(self):
        return self.institution if self.source == self.person else self.person

    def __str__(self):
        return '[%s, %s]' % (self.source, self.destination)

    class Meta:
        abstract = True


class InstitutionToInstitutionPartnership(InstitutionToInstitutionRelation):
    class Meta:
        verbose_name = _('Institution Partnership')
        verbose_name_plural = _('Institution Partnerships')


class PersonToPersonPartnership(PersonToPersonRelation):
    class Meta:
        verbose_name = _('Person Partnership')
        verbose_name_plural = _('Person Partnerships')


class PersonToInstitutionPartnership(PersonToInstitutionRelation):
    class Meta:
        verbose_name = _('Person To Institution Partnership')
        verbose_name_plural = _('Person To Institution Partnerships')


class InstitutionToInstitutionInvitation(InstitutionToInstitutionRelation):
    accepted = models.BooleanField(default=False)
    read = models.BooleanField(default=False)

    def accept(self):
        InstitutionToInstitutionPartnership.objects.create(
            source=self.source, destination=self.destination
        )
        self.accepted = True
        return self.close()

    def disable(self):
        self.accepted = False
        return self.close()

    class Meta:
        verbose_name = _('Institution Invitation')
        verbose_name_plural = _('Institution Invitations')


class PersonToPersonInvitation(PersonToPersonRelation):
    accepted = models.BooleanField(default=False)
    read = models.BooleanField(default=False)

    def accept(self):
        PersonToPersonPartnership.objects.create(
            source=self.source, destination=self.destination
        )
        self.accepted = True
        return self.close()

    def disable(self):
        self.accepted = False
        return self.close()

    class Meta:
        verbose_name = _('Person Invitation')
        verbose_name_plural = _('Person Invitations')


class PersonToInstitutionInvitation(PersonToInstitutionRelation):
    accepted = models.BooleanField(default=False)
    read = models.BooleanField(default=False)

    def accept(self):
        PersonToInstitutionPartnership.objects.create(
            institution=self.institution, person=self.person,
            institution_requested=self.institution_requested
        )
        self.accepted = True
        return self.close()

    def disable(self):
        self.accepted = False
        return self.close()

    class Meta:
        verbose_name = _('Person To Institution Invitation')
        verbose_name_plural = _('Person To Institution Invitations')
