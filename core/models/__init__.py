from .education_process import (
    PupilsClass, StudentsCourse,
    PersonActivity, PaymentPersonActivity, TrainingSessionPersonActivity,
    PersonEventRequirementsActivity, EducationPeriodEndPersonActivity,
    EducationPeriodPersonActivity
)
from .general import Page, Email, Phone
from .grading import Grade, GradingSystem
from .institutions import (
    EducationalInstitution, HighSchool, Campus, Speciality,
    School, College, Institute, University, HighSchoolDepartment,
    Subject, LectureRoom, Scholarship
)
from .persons import (
    User, Person, Pupil, Student, Teacher, PersonRole, person_photos,
    PersonNetworkStatus
)
from .persons_groups import PupilsGroup, StudentsGroup, PersonsGroup
from .relations import (
    InstitutionToInstitutionPartnership, InstitutionToInstitutionInvitation
)
from .timetable_events import (
    TimetableEvent, TrainingSession, Vacation, TimetableGeneralEvent,
    KnowledgeControl, TimetableEventsScheme, TimetableEventSlot,
    EventAbsenceReason, Payment, EducationPeriodEnd, EducationPeriod
)
