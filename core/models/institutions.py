from __future__ import unicode_literals, division
from polymorphic.models import PolymorphicModel
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.db import models
from django.apps import apps
from django.db.models import Q
from django.template.loader import render_to_string
from django.urls import reverse
from colorfield.fields import ColorField
from django.conf import settings
from django_countries.fields import CountryField
from .education_process import StudentsCourse
from .timetable_events import TimetableEvent
from .relations import (
    InstitutionToInstitutionPartnership, InstitutionToInstitutionInvitation
)
from django.utils.timezone import now
from django_google_maps.fields import AddressField, GeoLocationField


@python_2_unicode_compatible
class EducationalInstitution(PolymorphicModel):
    country = CountryField(verbose_name=_('Country'), null=True)
    address = AddressField(max_length=255, null=True, blank=True, verbose_name=_('Address'))
    geolocation = GeoLocationField(max_length=100, null=True, blank=True, verbose_name=_('Geolocation'))
    name = models.CharField(max_length=256, verbose_name=_('Name'), null=True)
    short_name = models.CharField(max_length=256, verbose_name=_('Short name'), null=True)
    slug = models.SlugField(verbose_name=_('Slug'), unique=True, null=True)
    rating = models.IntegerField(verbose_name=_('Rating'), default=0)

    emails = models.ManyToManyField('core.Email', verbose_name=_('Emails'), blank=True)
    phones = models.ManyToManyField('core.Phone', verbose_name=_('Phones'), blank=True)
    admins = models.ManyToManyField('core.Person', verbose_name=_('Admins'), blank=True)

    color = ColorField(default='#47bac1')

    scheme = models.ForeignKey('core.TimetableEventsScheme', verbose_name=_('Scheme'),
                               help_text=_('Default events scheme'),
                               related_name='institutions', on_delete=models.CASCADE, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    INSTITUTIONS_REGISTERED = ['School', 'College', 'Institute', 'University']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('institutions', args=[self.pk])

    @classmethod
    def create(cls, model_name, **kwargs):
        if model_name not in cls.INSTITUTIONS_REGISTERED:
            raise ValueError('Institution model must be in %s' % cls.INSTITUTIONS_REGISTERED)
        model = apps.get_model(app_label='core', model_name=model_name)
        model.objects.create(**kwargs)

    def get_campuses(self):
        return self.campuses.filter(active=True)

    def get_peoples(self):
        return self.persons.filter(active=True)

    def get_lecture_rooms(self):
        return self.lecture_rooms.filter(active=True)

    def get_subjects(self):
        return self.subjects.filter(active=True)

    def invite_unregistered(self, email):
        context = {
            'institution': self
        }
        send_mail(
            _('Join Us'),
            render_to_string('invite-unregistered.txt', context),
            settings.FROM_EMAIL,
            [email],
            html_message=render_to_string('invite-unregistered.html', context),
            fail_silently=False,
        )

    def create_invitation(self, pk):
        result = InstitutionToInstitutionInvitation.objects.get_or_create(
            source=self,
            destination=EducationalInstitution.objects.get(pk=pk)
        )
        return result[0]

    @property
    def outgoing_invitations(self):
        return InstitutionToInstitutionInvitation.objects\
            .filter(source=self, opened=True)

    @property
    def incoming_invitations(self):
        return InstitutionToInstitutionInvitation.objects\
            .filter(destination=self, opened=True)

    @property
    def partnerships(self):
        qs = InstitutionToInstitutionPartnership.objects\
            .filter(Q(destination=self) | Q(source=self), Q(opened=True))
        for item in qs:
            if item.source == self:
                item.partner = item.destination
            else:
                item.partner = item.source
        return qs

    def get_outgoing_invitation(self, pk):
        try:
            return InstitutionToInstitutionInvitation.objects\
                .get(pk=pk, source=self, opened=True)
        except InstitutionToInstitutionInvitation.DoesNotExist:
            return

    def get_incoming_invitation(self, pk):
        try:
            return InstitutionToInstitutionInvitation.objects\
                .get(pk=pk, destination=self, opened=True)
        except InstitutionToInstitutionInvitation.DoesNotExist:
            return

    def get_partnership(self, pk):
        try:
            return InstitutionToInstitutionPartnership.objects\
                .filter(Q(destination=self) | Q(source=self)).get(pk=pk, opened=True)
        except InstitutionToInstitutionPartnership.DoesNotExist:
            return

    def accept_invitation(self, pk):
        invitation = self.get_incoming_invitation(pk)
        if invitation:
            return invitation.accept()

    def refuse_invitation(self, pk):
        invitation = self.get_incoming_invitation(pk)
        if invitation:
            return invitation.disable()

    def recall_invitation(self, pk):
        invitation = self.get_outgoing_invitation(pk)
        if invitation:
            return invitation.disable()

    def disable_partnership(self, pk):
        partnership = self.get_partnership(pk)
        if partnership:
            return partnership.close()

    def create_training_sessions_timetable(self, *args):
        """
        Creates a new training sessions timetable.
        :param args: tuples of dicts
             {
                 'subject': subject,
                 'date_range': date_range,
                 'room': room,
                 'hours': hours,
                 'is_include': is_include
             }
        :return: 
        """
        if not args:
            args = [
                {
                    'subject': subject,
                    'date_range': None,
                    'room': None,  # ToDo: set random room
                    'hours': None,
                    'is_include': True
                } for subject in self.get_subjects()
            ]
        events = []
        for item in args:
            start, end = item['date_range']
            if not item['subject']:
                raise ValueError('Subject is required')
            if not item['room']:
                raise ValueError('Room is required')
            event = TimetableEvent(
                institution=self.institution,
                start=start,
                end=end,
                room=item['room'],
                color=item['subject'].color,
                presence_required=True
            )
            if event.is_period_available():
                events.append(event)
        TimetableEvent.objects.bulk_create(events)

    class Meta:
        verbose_name = _('Educational Institution')
        verbose_name_plural = _('Educational Institutions')


class HighSchool(EducationalInstitution):
    ACCREDITATION_LEVELS = [
        [1, 'I'],
        [2, 'II'],
        [3, 'III'],
        [4, 'IV']
    ]

    @property
    def departments(self):
        return self.highschooldepartment_set.filter(active=True)


class School(EducationalInstitution):
    def get_classes(self):
        return self.classes.filter(active=True)

    class Meta:
        verbose_name = _('School')
        verbose_name_plural = _('Schools')


class College(HighSchool):
    accreditation = models.IntegerField(
        verbose_name=_('Accreditation'),
        choices=HighSchool.ACCREDITATION_LEVELS,
        default=2
    )

    class Meta:
        verbose_name = _('College')
        verbose_name_plural = _('Colleges')


class Institute(HighSchool):
    accreditation = models.IntegerField(
        verbose_name=_('Accreditation'),
        choices=HighSchool.ACCREDITATION_LEVELS,
        default=3
    )

    class Meta:
        verbose_name = _('Institute')
        verbose_name_plural = _('Institutes')


class University(HighSchool):
    accreditation = models.IntegerField(
        verbose_name=_('Accreditation'),
        choices=HighSchool.ACCREDITATION_LEVELS,
        default=4
    )

    class Meta:
        verbose_name = _('University')
        verbose_name_plural = _('Universities')


@python_2_unicode_compatible
class Campus(models.Model):
    number = models.IntegerField(verbose_name=_('Number'))
    institution = models.ForeignKey('EducationalInstitution', verbose_name=_('Institution'), related_name='campuses',
                                    on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    description = models.TextField(verbose_name=_('Note'), null=True, blank=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return self.name

    def get_lecture_rooms(self):
        return self.lectureroom_set.filter(active=True)

    class Meta:
        verbose_name = _('Campus')
        verbose_name_plural = _('Campuses')


@python_2_unicode_compatible
class HighSchoolDepartment(models.Model):
    name = models.CharField(max_length=256, verbose_name=_('Name'))
    parent = models.ForeignKey('self', null=True, blank=True, verbose_name=_('Parent department'),
                               on_delete=models.CASCADE)
    institution = models.ForeignKey('EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE, null=True)
    active = models.BooleanField(verbose_name=_('Active'), default=True)

    def __str__(self):
        return self.name

    def departments_chain(self, obj=None):
        obj = obj or self
        if obj.parent:
            return self.departments_chain(obj.parent) + [obj]
        else:
            return [obj]

    def get_full_name(self, sep='/'):
        return sep.join([dep.name for dep in self.departments_chain()])

    def get_specialities(self):
        return self.specialities.filter(active=True)

    class Meta:
        verbose_name = _('Department')
        verbose_name_plural = _('Departments')


class Speciality(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    department = models.ForeignKey('HighSchoolDepartment', verbose_name=_('Department'), on_delete=models.CASCADE,
                                   related_name='specialities', null=True)
    institution = models.ForeignKey('EducationalInstitution', verbose_name=_('Institution'),
                                    on_delete=models.CASCADE, null=True)
    price = models.IntegerField(verbose_name=_('Price'), default=0)
    active = models.BooleanField(verbose_name=_('Active'), default=True)

    def get_courses(self):
        return self.courses.filter(active=True)

    def get_subjects(self, level_number):
        courses = self.get_courses()
        if courses:
            try:
                level = courses.get(number=level_number)
                return level.subjects.filter(active=True)
            except StudentsCourse.DoesNotExist:
                pass
        return Subject.objects.none()

    class Meta:
        verbose_name = _('Speciality')
        verbose_name_plural = _('Specialities')


@python_2_unicode_compatible
class Subject(models.Model):
    institution = models.ForeignKey('EducationalInstitution', verbose_name=_('Institution'), on_delete=models.CASCADE,
                                    related_name='subjects', null=True)
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    description = models.TextField(verbose_name=_('Note'), null=True, blank=True)
    color = ColorField(default='#47bac1')
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def get_teachers(self):
        return self.teacher_set.filter(active=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')


@python_2_unicode_compatible
class LectureRoom(models.Model):
    institution = models.ForeignKey('EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='lecture_rooms', on_delete=models.CASCADE, null=True)
    campus = models.ForeignKey('Campus', verbose_name=_('Campus'), on_delete=models.CASCADE, null=True)
    chief = models.ForeignKey('core.Teacher', verbose_name=_('Chief'), on_delete=models.CASCADE, null=True, blank=True)
    number = models.IntegerField(verbose_name=_('Number'))
    note = models.TextField(verbose_name=_('Note'), null=True, blank=True)
    free = models.BooleanField(verbose_name=_('Free'), default=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    @classmethod
    def get_free(cls, institution, datetime_range=None):
        if not datetime_range:
            return cls.objects.filter(active=True, free=True)

        if len(datetime_range) != 2:
            raise ValueError('Datetime range must have 2 values')

        datetime1, datetime2 = datetime_range

        if all(datetime_range):
            events = TimetableEvent.objects.filter(
                institution=institution,
                start__range=datetime_range,
                end__range=datetime_range,
                active=True
            )
            taken_up_rooms = [event.room for event in events if event.room]
            return cls.objects.filter(institution=institution, active=True) \
                .exclude(pk__in=[room.pk for room in taken_up_rooms])
        elif datetime1:
            datetime1 = now() if datetime1 < now() else datetime1
            events = TimetableEvent.objects.filter(
                institution=institution,
                end__gte=datetime1,
                active=True
            )
            taken_up_rooms = [event.room for event in events if event.room]
            return cls.objects.filter(institution=institution, active=True) \
                .exclude(pk__in=[room.pk for room in taken_up_rooms])
        elif datetime2:
            return cls.get_free(now(), datetime2)
        else:
            return cls.objects.filter(institution=institution, active=True, free=True)

    def __str__(self):
        return str(self.number)

    class Meta:
        verbose_name = _('Lecture Room')
        verbose_name_plural = _('Lecture Rooms')


class ScholarshipScheme(PolymorphicModel):
    institution = models.ForeignKey('EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='scholarship_schemes', on_delete=models.CASCADE)
    grading_system = models.ForeignKey('core.GradingSystem', verbose_name=_('Grading System'), null=True,
                                       on_delete=models.CASCADE, related_name='scholarship_schemes')
    name = models.IntegerField(verbose_name=_('Name'), default=0)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return self.name

    def get_scholarships(self, **kwargs):
        return self.scholarships.filter(active=True, **kwargs)

    def get_final_grade(self, grades):
        raise NotImplementedError

    def get_scholarship(self, grades):
        return self.get_scholarships(grade=self.get_final_grade(grades))

    class Meta:
        verbose_name = _('Scholarship Scheme')
        verbose_name_plural = _('Scholarship Schemes')


class Scholarship(models.Model):
    institution = models.ForeignKey('EducationalInstitution', verbose_name=_('Institution'),
                                    related_name='scholarships', on_delete=models.CASCADE)
    scheme = models.ForeignKey('ScholarshipScheme', verbose_name=_('Scheme'),
                               related_name='scholarships', on_delete=models.CASCADE, null=True)
    value = models.IntegerField(verbose_name=_('Value'), default=0)
    level = models.CharField(max_length=128, verbose_name=_('Level'))
    grade = models.ForeignKey('core.Grade', verbose_name=_('Grade'), on_delete=models.CASCADE, null=True)
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    def __str__(self):
        return str(self.value)

    class Meta:
        ordering = ['value']
        verbose_name = _('Scholarship')
        verbose_name_plural = _('Scholarships')
