from django.contrib import admin
from django import forms
from guardian.admin import GuardedModelAdmin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from ..models import (
    School, College, Institute, University, HighSchool,
    InstitutionToInstitutionInvitation, InstitutionToInstitutionPartnership,
    Pupil, Student, Teacher, PupilsGroup, StudentsGroup, PupilsClass, StudentsCourse,
    HighSchoolDepartment, Speciality, Subject, Grade, PersonActivity,
    Email, Phone, LectureRoom, Page, Campus, PersonRole, User, GradingSystem,
    TrainingSession, KnowledgeControl, Vacation, TimetableGeneralEvent,
    TimetableEventsScheme, TimetableEventSlot, EventAbsenceReason, Payment, EducationPeriodEnd,
    PaymentPersonActivity, TrainingSessionPersonActivity, PersonNetworkStatus,
    PersonEventRequirementsActivity, EducationPeriodEndPersonActivity
)
from django_google_maps import widgets as map_widgets
from django_google_maps import fields as map_fields
from sorl.thumbnail.admin import AdminImageMixin


@admin.register(PersonActivity)
class PersonActivityAdmin(admin.ModelAdmin):
    list_display = ['person', 'event', 'present']
    list_filter = ['institution', 'person', 'event', 'present']


@admin.register(PaymentPersonActivity)
class PaymentPersonActivityAdmin(admin.ModelAdmin):
    pass


@admin.register(TrainingSessionPersonActivity)
class TrainingSessionPersonActivityAdmin(admin.ModelAdmin):
    pass


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'default_profile')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


@admin.register(TimetableEventsScheme)
class TimetableEventsSchemeAdmin(admin.ModelAdmin):
    class TimetableEventSlotsInline(admin.TabularInline):
        model = TimetableEventSlot
        extra = 0
    inlines = [TimetableEventSlotsInline]


@admin.register(Campus)
class CampusAdmin(admin.ModelAdmin):
    class LectureRoomsInline(admin.TabularInline):
        model = LectureRoom
        extra = 0
    inlines = [LectureRoomsInline]


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    pass


class PersonsGroupAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'count']
    list_filter = ['institution']

    def count(self, obj):
        return obj.get_members().count()


@admin.register(PupilsGroup)
class PupilsGroupAdmin(PersonsGroupAdmin):
    class GroupPartnershipInline(admin.TabularInline):
        model = PupilsGroup.members.through
        extra = 1
    inlines = [GroupPartnershipInline]


@admin.register(StudentsGroup)
class StudentsGroupAdmin(PersonsGroupAdmin):
    class GroupPartnershipInline(admin.TabularInline):
        model = StudentsGroup.members.through
        extra = 1
    inlines = [GroupPartnershipInline]


@admin.register(LectureRoom)
class LectureRoomAdmin(admin.ModelAdmin):
    pass


@admin.register(Phone)
class PhoneAdmin(admin.ModelAdmin):
    pass


@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    pass


class TimetableEventAdmin(admin.ModelAdmin):
    list_display = ['__str__']


@admin.register(TrainingSession)
class TrainingSessionAdmin(TimetableEventAdmin):
    readonly_fields = ['name']


@admin.register(Vacation)
class VacationAdmin(TimetableEventAdmin):
    readonly_fields = ['name']


@admin.register(KnowledgeControl)
class KnowledgeControlAdmin(TimetableEventAdmin):
    readonly_fields = ['name']


@admin.register(TimetableGeneralEvent)
class TimetableGeneralEventAdmin(TimetableEventAdmin):
    pass


@admin.register(Payment)
class PaymentAdmin(TimetableEventAdmin):
    pass


@admin.register(GradingSystem)
class GradingSystemAdmin(admin.ModelAdmin):
    list_filter = ['name', 'description', 'active']

    class GradesInline(admin.TabularInline):
        model = Grade
        extra = 0

    inlines = [GradesInline]


class PersonAdmin(AdminImageMixin, admin.ModelAdmin):
    list_filter = ['institution']
    filter_horizontal = ['emails', 'phones']

    class PersonActivityInline(admin.TabularInline):
        model = PersonActivity
        extra = 0

    class PersonRoleInline(admin.TabularInline):
        model = PersonRole
        extra = 0

    class PersonNetworkStatusInline(admin.TabularInline):
        model = PersonNetworkStatus
        extra = 0

    inlines = [PersonActivityInline, PersonRoleInline, PersonNetworkStatusInline]


@admin.register(Pupil)
class PupilAdmin(PersonAdmin):
    pass


@admin.register(Student)
class StudentAdmin(PersonAdmin):
    pass


@admin.register(Teacher)
class TeacherAdmin(PersonAdmin):
    filter_horizontal = PersonAdmin.filter_horizontal[:]
    filter_horizontal += ['subjects']

    def get_form(self, request, obj=None, **kwargs):
        form = super(TeacherAdmin, self).get_form(request, obj=obj, **kwargs)
        if obj:
            form.base_fields['subjects'].queryset = \
                form.base_fields['subjects'].queryset.filter(institution=obj.institution)
        return form


@admin.register(HighSchoolDepartment)
class HighSchoolDepartmentAdmin(admin.ModelAdmin):
    list_display = ['name', 'full_name', 'active']
    list_filter = ['institution']

    def full_name(self, obj):
        return obj.get_full_name()


@admin.register(Speciality)
class SpecialityAdmin(admin.ModelAdmin):
    list_filter = ['institution']


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_filter = ['institution']


class EducationLevelAdmin(admin.ModelAdmin):
    filter_horizontal = ['subjects']
    list_filter = ['institution']


@admin.register(PupilsClass)
class PupilsClassAdmin(EducationLevelAdmin):
    pass


@admin.register(StudentsCourse)
class StudentsCourseAdmin(EducationLevelAdmin):
    pass


@admin.register(InstitutionToInstitutionPartnership)
class InstitutionToInstitutionPartnershipAdmin(admin.ModelAdmin):
    list_display = ['source', 'destination', 'created']


@admin.register(InstitutionToInstitutionInvitation)
class InstitutionToInstitutionInvitationAdmin(admin.ModelAdmin):
    list_display = ['source', 'destination', 'opened', 'accepted', 'created']


class EducationalInstitutionAdmin(admin.ModelAdmin):
    list_display = ['name', 'rating', 'active']
    filter_horizontal = ['emails', 'phones']
    prepopulated_fields = {'slug': ('name', )}
    formfield_overrides = {
        map_fields.AddressField: {'widget': map_widgets.GoogleMapsAddressWidget},
    }

    class CampusesInline(admin.TabularInline):
        model = Campus
        extra = 0

    inlines = [CampusesInline]


class HighSchoolAdmin(EducationalInstitutionAdmin):
    filter_horizontal = ['emails', 'phones']


@admin.register(School)
class SchoolAdmin(EducationalInstitutionAdmin):
    pass


@admin.register(College)
class CollegeAdmin(HighSchoolAdmin):
    pass


@admin.register(Institute)
class InstituteAdmin(HighSchoolAdmin):
    pass


@admin.register(University)
class UniversityAdmin(HighSchoolAdmin):
    pass
