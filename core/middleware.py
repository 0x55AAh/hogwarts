from django.utils.deprecation import MiddlewareMixin


class ProfileMiddleware(MiddlewareMixin):
    def process_request(self, request):
        profile, institution = None, None
        if not request.user.is_anonymous():
            profile = request.user.get_current_profile()
            profile.network_status.online()
            institution = getattr(profile, 'institution', None)
        request.profile = profile
        request.institution = institution
