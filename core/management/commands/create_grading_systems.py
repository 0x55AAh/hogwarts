from django.core.management.base import BaseCommand, CommandError
from core.models.grading import p5, p12, pass_fail, ects


class Command(BaseCommand):
    help = 'Creates grading systems with grades'
    grading_systems_creators = [p5, p12, pass_fail, ects]

    def handle(self, *args, **options):
        for creator in self.grading_systems_creators:
            creator()
