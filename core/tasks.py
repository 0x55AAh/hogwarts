from datetime import timedelta
from django.utils import timezone
from hogwarts.celery import app
from celery.utils.log import get_task_logger
from celery.schedules import crontab

logger = get_task_logger(__name__)


@app.task(ignore_result=True)
def force_set_profiles_offline():
    from .models import PersonNetworkStatus
    qs = PersonNetworkStatus.objects.filter(
        updated__lte=timezone.now() - timedelta(minutes=2),
        value=PersonNetworkStatus.STATUS.online
    )
    count = qs.count()
    for network_status in qs:
        network_status.offline()
    logger.info('Profiles network statuses force updated (%d).' % count)


app.conf.beat_schedule = {
    'force_set_profiles_offline': {
        'task': 'core.tasks.force_set_profiles_offline',
        'schedule': crontab(minute='*/2'),
    },
}
