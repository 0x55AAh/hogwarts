def core(request):
    return {
        'user': request.user,
        'profile': request.profile,
        'institution': request.institution,
        'new_messages': request.profile.new_messages if request.profile else []
    }
