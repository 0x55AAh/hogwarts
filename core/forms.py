from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.utils.text import slugify
from registration.forms import RegistrationForm
from .models import EducationalInstitution


class InstitutionRegistrationForm(RegistrationForm):
    INSTITUTIONS = [(slugify(i), i) for i in EducationalInstitution.INSTITUTIONS_REGISTERED]
    institution = forms.ChoiceField(label=_('Institution'), choices=INSTITUTIONS, initial='college')

    def __init__(self, *args, **kwargs):
        self._institutions_dict = dict(self.INSTITUTIONS)
        super(InstitutionRegistrationForm, self).__init__(*args, **kwargs)

    def get_institution_model_name(self):
        return self._institutions_dict[self.institution]


class ProfileForm(forms.ModelForm):
    class Meta:
        model = EducationalInstitution
        fields = '__all__'
